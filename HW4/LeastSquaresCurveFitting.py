"""
Luke Vaughan
2-24-20
HW4
"""
"""
Write a program that demonstrates the Least Squares Curve Fitting method. You must write and call at least the following
3 functions.
    def LeastSquares(x,y,power): calculates and returns an array containing the coefficients of the least squares 
    polynomial
    
    def PlotLeastSquares(x,y,power,showpoints = True, npoints = 500): which calls LeastSquares, generates data points 
    andplots the least squares curve. If showpoints is True, also put the original data on the same plot.
    
    def main():
        A main program that uses the data given to:
        1) Call LeastSquares to generate and print the coefficients of a Linear fit.
        2) Call PlotLeastSquares to display a plot for the Linear fit.
        3) Call Least Squares to display a plot for the Cubic fit.
        4) Call PlotLeastSquares to display a plot for the Cubic fit.
        5) Uses the results from 1 and 3 to plot the data points, the Linear fit and the Cubic fit, all on one graph. 
        Use proper titles, labels and legends.
"""
# Imports needed libraries
import numpy as np
import matplotlib.pyplot as plt

def LeastSquares(x,y,power):
    # Finds the max power of x sums that we must find
    sx_max = power * 2
    # Declares and Initializes some numpy matrices and arrays
    sx = np.zeros(sx_max + 1)
    b = np.zeros((power + 1, 1))
    A = np.zeros((power + 1, power + 1))

    # Iterates through the values to find their sum to the i-th power
    for i in range(sx_max + 1):
        sx[i] = np.sum(np.power(x,i))

    # Iterates through the values to find the product of y with x to the power of i.
    for i in range(power + 1):
        b[i,0] = np.sum(np.multiply(y,np.power(x,i)))

    # Forms the matrix of the sx values
    offset = 0
    for row in range(power + 1):
        for col in range(power + 1):
            A[row, col] = sx[col + offset]
        offset += 1

    # Augments A and b to send to GaussElim
    z = np.hstack((A,b))
    # Note z must be a list and not a numpy ndarray. Must use .tolist() function to convert to correct data type!
    # Returns the coefficients of Least Squares
    return np.linalg.solve(A,b)
    # return GaussElim(z.tolist())

def PlotLeastSquares(x, y, power, showpoints = True, npoints = 500):
    # Calls the LeastSquares function to get the coefficients of the polynomial
    coeff = LeastSquares(x, y, power)

    # Initializes u and v
    u = np.linspace(min(x), max(x), npoints)
    v = np.zeros_like(u)

    # Determines the value of v given the coefficients and u value
    for i in range(len(u)):
        for j in range(len(coeff)):
            v[i] += coeff[j] * (u[i]) ** j

    # plots u and v in a green line
    plt.plot(u,v,'g')

    # Determines if it should show the original data points and adds the correct legend
    if showpoints == True:
        plt.plot(x,y,'bs')
        plt.legend(['Least Squares Curve Fit','Data Points'])
    else:
        plt.legend(['Least Squares Curve Fit'])

    # Labels the x and y axes
    plt.xlabel('x')
    plt.ylabel('y')

    # Adds a title to the plot
    plt.title('Least Squares Curve Fit with a polynomial of degree {:d}.'.format(power))

    # Shows the plot in a figure window
    plt.show()

def PlotLeastSquaresBoth(x, y, power1, power2, showpoints = True, npoints = 500):
    # Calls the LeastSquares function to get the coefficients of the polynomial
    coeff1 = LeastSquares(x, y, power1)
    coeff2 = LeastSquares(x, y, power2)

    # Initializes u1 v1 and u2 v2
    u1 = np.linspace(min(x), max(x), npoints)
    v1 = np.zeros_like(u1)
    u2 = np.linspace(min(x), max(x), npoints)
    v2 = np.zeros_like(u2)

    # Determines the value of v given the coefficients and u value
    for i in range(len(u1)):
        for j in range(len(coeff1)):
            v1[i] += coeff1[j] * (u1[i]) ** j

    for i in range(len(u2)):
        for j in range(len(coeff2)):
            v2[i] += coeff2[j] * (u2[i]) ** j

    # plots u and v in a green line
    plt.plot(u1, v1, 'g')
    plt.plot(u2, v2, 'r')

    # Determines if it should show the original data points and adds the correct legend
    if showpoints == True:
        plt.plot(x, y, 'bs')
        plt.legend(['Least Squares Curve Fit of degree {:d}'.format(power1), 'Least Squares Curve Fit of degree {:d}'.format(power2), 'Data Points'])
    else:
        plt.legend(['Least Squares Curve Fit of degree {power1}', 'Least Squares Curve Fit of degree {power2}'])

    # Labels the x and y axes
    plt.xlabel('x')
    plt.ylabel('y')

    # Adds a title to the plot
    plt.title('Least Squares Curve Fit with a polynomial of degree {:d} and {:d}.'.format(power1, power2))

    # Shows the plot in a figure window
    plt.show()


def main():
    x = np.array([0.05, 0.11, 0.15, 0.31, 0.46, 0.52, 0.7, 0.74, 0.82, 0.98, 1.17])
    y = np.array([0.956, 1.09, 1.332, 0.717, 0.771, 0.539, 0.378, 0.370, 0.306, 0.242, 0.104])

    # prints the coefficients of the least squares polynomial
    print(LeastSquares(x, y, 1))
    print(LeastSquares(x, y, 3))

    # Plots the graph of polynomial of degree n
    PlotLeastSquares(x, y, 1)
    PlotLeastSquares(x, y, 3)

    # Plots both of the graphs (This isn't very efficient but it fits the problem statement)
    PlotLeastSquaresBoth(x, y, 1, 3)

main()