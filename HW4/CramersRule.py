"""
Luke Vaughan
2-24-20
HW4
"""
"""
Write a program that demonstrates the use of Cramer's Rule to solve a set of linear equations, entered in matrix form.
    def Cramer(A, b) returns the solution to A x = b using Cramer's Rule
    
    def Determinant(A) which uses the Rule of Minors to calculate and return the determinant of A.
    
    def Submatrix(A,j,k) which returns the remaining submatrix after removing row j and column k.
"""
# Imports necessary libraries
import numpy as np
import copy

def Submatrix(A,j,k):
    # Creates a copy of A and stores it in a different location in memory
    c = copy.deepcopy(A)
    # Deletes the j th row
    c = np.delete(c,j,0)
    # Deletes the k th column
    c = np.delete(c,k,1)
    return c

def Determinant(A):
    # Declares and Initializes the value of the determinant
    det = 0
    # Checks if the submatix is a 1x1 and if it is, it will return the value
    if len(A) == 1:
        return A
    # Expands along the first row and iterates through the columns,
    # finds the submatrix and uses recursion to calculate determinate.
    for j in range(len(A)):
        submat = Submatrix(A,0,j)
        det += (-1) ** j * A[0,j] * Determinant(submat)
    # Returns value of determinant
    return det

def Cramer(A,b):
    # Initializes the det variable used for storing the determinants after replacing the necessary values.
    det = np.zeros(len(b))
    # Initializes the answer variable that returns the final answers
    answer = np.zeros(len(b))
    # Calls Determinant() function to find the determinant of matrix A
    deter = Determinant(A)
    # Loops through the variables and calculates each using Cramer's Rule
    for i in range(len(A)):
        # Creates a copy of A
        C = copy.deepcopy(A)
        # Replaces the ith column of C with the corresponding b values
        C[:,i] = np.transpose(b)
        # Calculates the determinant of the matrix with the replaced values
        det[i] = Determinant(C)
        # Finds the answer due to Cramer's Rule
        answer[i] = det[i]/deter
    # Returns answer
    return answer

def main():
    A = np.array([[1, -2, 3, 4], [5, 6, 7, 8], [-9, 10, -11, 6], [5, 4, -3, 2]])
    b = np.array([1, 2, 3, 4])
    print("\nThe submatrix is:\n", Submatrix(A, 1, 2))
    print("\nThe determinant of matrix A is:", Determinant(A))
    print("\nUsing Cramer's Rule, the answers are:\n", Cramer(A, b))

    A = np.array([[-5, 1, -5, 0, 1, -4], [5, 0, 3, 5, 3, 5], [-2, -2, 1, 4, 3, -5],
                  [4, 5, 0, 3, 4, -1], [-5, -2, -5, 5, -2, -2], [4, 5, 5, 0, 0, -2]])
    b = np.array([-95, -45, 49, -50, 90, 30])
    print("\nThe submatrix is:\n", Submatrix(A, 1, 2))
    print("\nThe determinant of matrix A is:", Determinant(A))
    print("\nUsing Cramer's Rule, the answers are:\n", Cramer(A, b))

main()