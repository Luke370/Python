"""
Luke Vaughan
2-24-20
HW4
"""
"""
Write a program that demonstrates the Cubic Spline method. You must write and call at least the following 3 functions:
    def CubicSpline(x,y,slope1 = 0, slope2 = 0): which calculates and returns a matrix containing the coefficients of 
    the cubic splines. Slope 1 and slope2 are the slopes at the first and last points.
    
    def PlotCubicSpline(x,y,slope1 = 0, slope2 = 0, showpoints = True, npoints = 500): which calls LeastSquares, 
    generates datapoints and plots the least square cruve. If showpoints is True, also put the original data on the 
    same plot.
    
    def main():
        1) Call CubicSpline to generate and print the coefficients
        2) Call PlotCubicSpline to display a plot of the cubic spline.
"""
import numpy as np
import matplotlib.pyplot as plt

def CubicSpline(x, y, slope1 = 0, slope2 = 0):
    # Declares useful constants
    N = len(x)
    h = x[1] - x[0]

    # Declares and Initializes the A matrix
    A = np.zeros((N,N))
    A[0,0] = 1
    A[N-1,N-1] = 1
    insert = np.array([1,4,1])
    for i in range(N-2):
        A[i+1,i:i+3] = insert

    # Declares and Initializes the b vector
    b = np.zeros((N, 1))
    b[0,0] = slope1
    b[N-1,0] = slope2
    for i in range(1,N-1):
        b[i,0] = (3/h) * (y[i+1] - y[i-1])

    # To solve for the values of k, we use numpy to solve A augmented with b.
    k = np.linalg.solve(A,b)

    # Declare and Initialize 'a' matrix to solve for coefficients of splines
    a = np.zeros((N-1,4))
    for i in range(N-1):
        a[i,0] = y[i]
        a[i,1] = k[i]
        a[i,2] = (3/h**2)*(y[i+1]-y[i]) - (1/h)*(k[i+1]+2*k[i])
        a[i,3] = (2/h**3)*(y[i]-y[i+1]) + (1/h**2)*(k[i+1]+k[i])

    # Returns the values of the coefficients of the splines
    return a

def PlotCubicSpline(x, y, slope1 = 0, slope2 = 0, showpoints = True, npoints = 500):
    coeff = CubicSpline(x, y, slope1, slope2)

    N = len(x)
    u = np.linspace(x[0], x[N-1], npoints)
    v = np.zeros((len(u)))

    m = 0
    for i in range(len(u)):
        for j in range(len(coeff[0,:])):
            if u[i] >= x[m+1] and m < len(x) - 2:
                m += 1
            v[i] += coeff[m,j] * (u[i]-x[m]) ** j

    # plots u and v in a green line
    plt.plot(u,v,'g--')

    # Determines if it should show the original data points and adds the correct legend
    if showpoints == True:
        plt.plot(x,y,'bs')
        plt.legend(['Cubic Splines','Data Points'])
    else:
        plt.legend(['Least Squares Curve Fit'])

    # Labels the x and y axes
    plt.xlabel('x')
    plt.ylabel('y')

    # Adds a title to the plot
    plt.title('Cubic Splines Through Data Points')

    # Shows the plot in a figure window
    plt.show()

def main():
    #x = np.array([0,2,4,6])
    #y = np.array([4,0,4,80])
    #slope1 = 0
    #slope2 = 20

    x = np.array([1.5, 3, 4.5, 6, 7.5, 9])
    y = np.array([3.5, 1.5, -2, 6.9, 8.2, 1.5])
    slope1 = 2
    slope2 = -4

    print(CubicSpline(x, y, slope1, slope2))
    PlotCubicSpline(x, y, slope1, slope2)

main()