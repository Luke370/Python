# Standard GUI Imports
import sys
from PyQt5.QtWidgets import QDialog, QApplication
from PyQt5.QtWidgets import QFileDialog, QMessageBox
from PyQt5.QtGui import QCursor
from PyQt5.QtCore import Qt

# Standard Plot Imports
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

# Other Imports
import numpy as np
from CubicSpline import CubicSpline


# ******************************************************************


# Standard Class definition for plotting
class PlotCanvas(FigureCanvas):
    def __init__(self, parent, width=None, height=None, dpi=100):
        if width == None: width = parent.width() / 100
        if height == None: height = parent.height() / 100
        fig = Figure(figsize=(width, height), dpi=dpi)
        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

    def plotit(self, x, y, slope1, slope2, showpoints = True, npoints = 500):

        self.figure.clf()

        coeff = CubicSpline(x, y, slope1, slope2)

        N = len(x)
        u = np.linspace(x[0], x[N - 1], npoints)
        v = np.zeros((len(u)))

        m = 0
        for i in range(len(u)):
            for j in range(len(coeff[0, :])):
                if u[i] >= x[m + 1] and m < len(x) - 2:
                    m += 1
                v[i] += coeff[m, j] * (u[i] - x[m]) ** j


        ax = self.figure.add_subplot(111)
        if showpoints == True:
            ax.plot(x, y, 'bo')
        ax.plot(u, v)
        ax.set_title('Cubic Spline')
        self.draw()

# Standard GUI Import
from CubicSplineGUI import Ui_Dialog

class main_window(QDialog):
    def __init__(self):
        # Standard Declarations
        super(main_window, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.assign_widgets()
        plotwin = self.ui.graphicsView
        self.m = PlotCanvas(plotwin)
        self.show()
        # My Declarations
        self.slope1 = None
        self.slope2 = None

    def assign_widgets(self):
        self.ui.pushButton_Exit.clicked.connect(self.ExitApp)
        self.ui.pushButton_Calc.clicked.connect(self.LoadandCalculate)

    def LoadandCalculate(self):
        # Read x and y and filename
        filename = str(self.ui.Filename_Box.text())
        x, y = np.loadtxt(filename, skiprows=1, unpack=True)
        n = len(x)

        # Check which radio box is checked and assign slope value
        if self.ui.RB_SpecifiedSlopes.isChecked():
            self.slope1 = float(self.ui.FirstSlope_Box.text())
            self.slope2 = float(self.ui.LastSlope_Box.text())
        elif self.ui.RB_ZeroSlopes.isChecked():
            self.slope1 = 0
            self.slope2 = 0
        elif self.ui.RB_StandardSlopes.isChecked():
            self.slope1 = (y[1] - y[0]) / (x[1] - x[0])
            self.slope2 = (y[n-1] - y[n-2]) / (x[n-1] - x[n-2])

        coeffs = CubicSpline(x, y, self.slope1, self.slope2)

        c = coeffs[0]
        firsteq = '{:.4f} + {:.4f}*(x-{:.4f}) + {:.4f}*(x-{:.4f})^2 + {:.4f}*(x-{:.4f})^3' \
            .format(c[0], c[1], x[0], c[2], x[0], c[3], x[0])

        c = coeffs[n-2]
        lasteq = '{:.4f} + {:.4f}*(x-{:.4f}) + {:.4f}*(x-{:.4f})^2 + {:.4f}*(x-{:.4f})^3' \
            .format(c[0], c[1], x[n-2], c[2], x[n-2], c[3], x[n-2])

        self.ui.FirstEquation.setText(firsteq)
        self.ui.LastEquation.setText(lasteq)
        self.m.plotit(x, y, self.slope1, self.slope2)
        return

    def ExitApp(self):
        app.exit()


if __name__ == "__main__":
    app = QApplication.instance()
    if not app:
        app = QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater)
    main_win = main_window()
    sys.exit(app.exec_())