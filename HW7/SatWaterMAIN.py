# Standard Imports
import sys
from PyQt5.QtWidgets import QDialog, QApplication
from PyQt5.QtWidgets import QFileDialog, QMessageBox
from PyQt5.QtGui import QCursor
from PyQt5.QtCore import Qt

# Imports the data from the UI file created by QT Designer
from QTSatWater import Ui_Dialog

# Imports SatSteam class from SatSteamClass.py
from SatSteamClass import SatSteam

class main_window(QDialog):
    # Standard Code
    def __init__(self):
        super(main_window, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.assign_widgets()
        self.show()

    def assign_widgets(self):
        # Customized to the buttons.
        # Exit button connects to the ExitApp Method. Calc button connects to the Calculate Method.
        self.ui.pushButton_Exit.clicked.connect(self.ExitApp)
        self.ui.pushButton_Calc.clicked.connect(self.Calculate)

    def Calculate(self):
        # Imports pressure and quality from the user through the line edit boxes.
        pressure = float(self.ui.Pressure_Box.text())
        quality = float(self.ui.Quality_Box.text())
        # Calls SatSteam class and assigns it a pressure and quality.
        SatWater = SatSteam(pressure, quality)

        # Fills in the following boxes with their respective output from the SatWater variable.
        self.ui.Temperature_Box.setText('{:.3f}'.format(SatWater.tsat))
        self.ui.Enthalpy_Box.setText('{:.3f}'.format(SatWater.h))
        self.ui.Entropy_Box.setText('{:.3f}'.format(SatWater.s))
        self.ui.SpecVolume_Box.setText('{:.5f}'.format(SatWater.v))
        return

    # Standard code to Close App
    def ExitApp(self):
        app.exit()

# Standard Main Command. Runs the GUI.
if __name__ == "__main__":
    app = QApplication.instance()
    if not app:
        app = QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater)
    main_win = main_window()
    sys.exit(app.exec_())