from scipy.interpolate import griddata
import numpy as np

class SatSteam:
    def __init__(self, psat, quality = None, name = None):
        self.psat = psat
        self.X = quality
        self.name = name
        self.tsat = None
        self.h = None
        self.s = None
        self.v = None
        if self.X is not None:  # Quality was given so update the other properties
            self.updateProperties()

    # set the Quality value and update the properties
    def Quality(self, quality):
        self.X = quality
        self.updateProperties()

    # with pressure and quality known, interpolate in the sat_water_table
    # and calculate the remaining properites
    def updateProperties(self):
        temp, pressure, hf, hg, sf, sg, vf, vg = np.loadtxt('sat_water_table.txt', skiprows=1, unpack=True)
        self.tsat = float(griddata((pressure), temp, (self.psat)))
        hf = float(griddata(pressure, hf, self.psat))
        hg = float(griddata(pressure, hg, self.psat))
        sf = float(griddata(pressure, sf, self.psat))
        sg = float(griddata(pressure, sg, self.psat))
        vg = float(griddata(pressure, vg, self.psat))
        vf = float(griddata(pressure, vf, self.psat))
        self.h = hf + self.X * (hg - hf)
        self.s = sf + self.X * (sg - sf)
        self.v = vf + self.X * (vg - vf)

    def print(self):
        print("Name:", self.name, "\n\tPsat:", self.psat, "bar", "\n\tQuality", self.X, "\n\tTsat:", self.tsat, "degC",
              "\n\tEnthalpy:", self.h, "kJ/kg", "\n\tEntropy:", self.s, "kJ/(kg K)", "\n\tVolume:", self.v, "m^3/kg", "\n")
def main():
    state1 = SatSteam(90, quality = 1.00, name = 'Turbine Inlet')
    state1.print()

    state2 = SatSteam(8, name = 'Turbine Exit')
    state2.Quality(0.7874)
    state2.print()

    state3 = SatSteam(8, quality = 0.0) # no name
    state3.print()

if __name__ == "__main__":
    main()

