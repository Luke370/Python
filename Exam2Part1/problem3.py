# Imports necessary libraries
from scipy.integrate import odeint
import matplotlib.pyplot as plt
import numpy as np

# Returns the derivatives used for odeint()
def ode_system(X, t, C1, C2, R, L):
    # Names the variables as the state variables
    v1 = X[0]
    i = X[1]
    v2 = X[2]

    # Calculates the derivatives
    v1dot = i / C1
    idot = -(v1+v2)/L
    v2dot = (i/C2) - v2/(R*C2)

    # Returns the value fo the derivatives
    return [v1dot, idot, v2dot]

def main():
    # Creates and initializes the time array
    t = np.linspace(0,0.00006,5000)

    # Sets the initial conditions
    ic = [-1, 0, 0]

    # The values for the constant parameters
    C1 = 100*10**(-9)
    C2 = C1
    R = 30
    L = 184*10**(-6)

    # Uses odeint() to get the solution for current and position over time
    x = odeint(ode_system, ic, t, args=(C1, C2, R, L))

    # Plots the solution for x(t) with nice title and labels
    plt.plot(t, x[:,0])
    plt.plot(t, x[:, 2])
    plt.title('Voltage  vs Time')
    plt.xlabel('Time, s')
    plt.ylabel('Voltage, V')
    plt.legend(('v1','v2'))
    plt.show()

main()