from scipy.optimize import fsolve
import math

def equations(vals, c1, c2):
    theta, z = vals
    func1 = math.cos(0.1*theta)*math.exp(-2*z)-c1*theta
    func2 = theta + z - c2
    return func1, func2

def main():

    guess = (0, 0)
    c1 = 3
    c2 = 1
    theta, z = fsolve(equations, guess, args=(c1, c2))
    print("When c1 =", c1, "\n\tThe value for theta is:", theta, "\n\tThe value for z is:", z)
    c1 = [2.5, 3.5, 4.5, 5.5]
    for i in range(len(c1)):
        theta, z = fsolve(equations, guess, args=(c1[i], c2))
        print("\nWhen c1 =", c1[i], "\n\tThe value for theta is:", theta,"\n\tThe value for z is:", z)

main()