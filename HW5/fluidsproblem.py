import math
from scipy.optimize import fsolve

def friction(D, Re, e = 0.00015):
    return (-2.0*math.log10(e/(3.7065*D) - 5.0452/Re * math.log10(1/2.8257 * (e/D)**1.1098 + 5.8506/Re**0.8981)))**(-2)

def Re(v, D, rho = 1.94, mu = 0.000019):
    return rho * abs(v) * D / mu

def PressureDrop(friction, L, v, D, rho = 1.94):
    return friction * L * abs(v) * v * rho / (2 * D)

def GetPressureDrop(Q, D, L):
    return PressureDrop(friction(D,Re(velocity(Q,D),D)),L,velocity(Q,D),D)

def velocity(Q, D):
    return 4 * Q / (math.pi * D**2)

def FlowEquations(vals):
    (Q0, Q1, Q2, Q3, Q4, Q5, Q6) = vals
    QE = 3
    QB = 1
    QC = 2
    f1val = Q3 - Q2
    f2val = QB - Q2 - Q1
    f3val = QC - Q6 - Q5
    f4val = Q4 -Q5
    f5val = Q1 + Q6 - Q0
    f6val = QE - Q3 - Q4 - Q0
    f7val = (GetPressureDrop(Q3, 6/12, 4000) + GetPressureDrop(Q2, 6/12, 3000)
            - GetPressureDrop(Q1, 8/12, 2000) - GetPressureDrop(Q0, 12/12, 2000))
    f1val = (GetPressureDrop(Q4, 8/12, 1000) + GetPressureDrop(Q5, 8/12, 3000)
            - GetPressureDrop(Q6, 8/12, 2000) - GetPressureDrop(Q0, 12/12, 2000))
    #f7val = (PressureDrop(friction(6/12,Re(velocity(Q3,6/12),6/12)),4000,velocity(Q3,6/12),6/12)
    #         + PressureDrop(friction(6/12,Re(velocity(Q2,6/12),6/12)),3000,velocity(Q2,6/12),6/12)
    #         - PressureDrop(friction(8/12,Re(velocity(Q1,8/12),8/12)),2000,velocity(Q1,8/12),8/12)
    #         - PressureDrop(friction(12/12,Re(velocity(Q0,12/12),12/12)),2000,velocity(Q0,12/12),12/12))
    """f7val = (PressureDrop(friction(5/12,Re(velocity(Qbc,5/12),5/12)),3500,velocity(Qbc,5/12),5/12)
             + PressureDrop(friction(6/12,Re(velocity(Qcd,6/12),6/12)),6000,velocity(Qcd,6/12),6/12)
             - PressureDrop(friction(8/12,Re(velocity(Qbd,8/12),8/12)),2000,velocity(Qbd,8/12),8/12))
    """
    return (f1val, f2val, f3val, f4val, f5val, f6val, f7val)

def main():
    (Q0, Q1, Q2, Q3, Q4, Q5, Q6) = fsolve(FlowEquations,(1,1,1,1,1,1,1))
    print(Q0,"\n", Q1,"\n", Q2,"\n", Q3,"\n", Q4,"\n", Q5,"\n", Q6)

    #(Q0, Q1, Q2, Q3, Q4, Q5, Q6) = fsolve(FlowEquations, (1, 2, 1, 1, 1, 1, 1))
    #print(Q0, Q1, Q2, Q3, Q4, Q5, Q6)

main()