"""
Luke Vaughan
3-2-20
HW5
"""
"""
Write a program that uses FSOLVE() to solve for the volumetric flow rate in each segment of the pipe flow network.
Use the following properties for water: density = 1.94 slug/ft^3, viscosity = 0.0000186 lbf-s/ft^2 and pipe roughness = 0.00082.

Your program should print the flow in each segment of pipe, nicely formatted, similar to:

The flow in segment a-b is -1.153 ft^3/s

Note: positive flow is directed from the lower letter to the higher letter.
"""
# Imports useful libraries
import math
from scipy.optimize import fsolve

# Defines the friction function which returns the friction in the pipe
def friction(D, Re, e = 0.00082):
    return (-2.0*math.log10(e/(3.7065*D) - 5.0452/Re * math.log10(1/2.8257 * (e/D)**1.1098 + 5.8506/Re**0.8981)))**(-2)

# Defines the Reynolds Number function which returns the reynolds number
def Re(v, D, rho = 1.94, mu = 0.0000186):
    return rho * abs(v) * D / mu

# Defines the pressure drop in the pipe which returns the pressure drop
def PressureDrop(friction, L, v, D, rho = 1.94):
    return friction * L * abs(v) * v * rho / (2 * D)

# Basically just reformatting the Pressure drop function to make it more user friendly.
def GetPressureDrop(Q, D, L):
    return PressureDrop(friction(D,Re(velocity(Q,D),D)),L,velocity(Q,D),D)

# Defines the velocity function which returns the velocity in the pipe
def velocity(Q, D):
    return 4 * Q / (math.pi * D**2)

# FlowEquations defines the set of equations for the system
def FlowEquations(vals):
    # Initial guesses
    (Qab, Qbc, Qcd, Qde, Qae, Qbd) = vals

    # Junction Rule at point A
    f1val = Qab + Qae - 2

    # Junction Rule at point B
    # NOTE: This equation was arbitrarily excluded because the system was chaotic if
    # both pressure equations weren't in the set.
    # f2val = Qab - Qbc - Qbd

    # Junction Rule at point E
    f3val = Qde + Qae - 0.6

    # Junction Rule at D
    f4val = Qcd - Qde + Qbd

    # Junction Rule at C
    f5val = Qbc - Qcd - 1.4

    # Loop Rule for ABDE
    f6val = (GetPressureDrop(Qab, 4/12, 2500) + GetPressureDrop(Qbd, 8/12, 2000)
             + GetPressureDrop(Qde, 8/12, 1500) - GetPressureDrop(Qae,4/12,3000))

    # Loop Rule for BCD
    f2val = (GetPressureDrop(Qbc, 5/12, 3500) + GetPressureDrop(Qcd, 6/12, 6000) - GetPressureDrop(Qbd,8/12,2000))

    # Returns function values
    return (f1val, f2val, f3val, f4val, f5val, f6val)

def main():
    (Qab, Qbc, Qcd, Qde, Qae, Qbd) = fsolve(FlowEquations,(1,1,1,1,1,1))
    print("The flow in segment a-b is", Qab, "ft^3/s")
    print("The flow in segment b-c is", Qbc, "ft^3/s")
    print("The flow in segment c-d is", Qcd, "ft^3/s")
    print("The flow in segment d-e is", Qde, "ft^3/s")
    print("The flow in segment a-e is", Qae, "ft^3/s")
    print("The flow in segment b-d is", Qbd, "ft^3/s")

main()