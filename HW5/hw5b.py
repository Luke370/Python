"""
Luke Vaughan
3-2-20
HW5
"""
"""
Write a program that uses FSOLVE() to solve the Rolling Wheel Dynamics - impending slip problem shown on canvas. 
You must use the "args" feature of FSOLVE to communicate the wheel parameters. Use all of the wheel parameters 
posted, except for the static coefficient of friction and the ramp-anlge, theta. Print the maximum no-slip ramp 
angles associated with u_s = 0.29 and u_s = 0.65. 
"""

# Imports necessary libraries
import math
from scipy.optimize import fsolve
import numpy as np
import matplotlib.pyplot as plt

# Defines the equations to send to FSOLVE()
def RollEquations(vals, args=[30,32.2,1.5,0.8,0.25]):
    (W, gc, r, kg, mus) = args
    (F, N, ag, alpha, theta) = vals
    f1val = W/gc * ag + F - W * math.sin(theta)
    f2val = N - W * math.cos(theta)
    f3val = W/gc * kg**2 * alpha - F * r
    f4val = r * alpha - ag
    f5val = mus * N - F
    return(f1val, f2val, f3val, f4val, f5val)

def main():
    # Performs fsolve() for mu_s = 0.25
    (F, N, ag, alpha, theta) = fsolve(RollEquations, (0,0,0,0,0), args = [30,32.2,1.5,0.8,0.25])
    #print(F, N, ag, alpha, theta)
    print("The maximum no-slip ramp angle associated with mu_s = 0.25 is", theta*180/math.pi, "degrees")

    # Performs fsolve() for mu_s = 0.65
    (F, N, ag, alpha, theta) = fsolve(RollEquations, (0, 0, 0, 0, 0), args=[30, 32.2, 1.5, 0.8, 0.65])
    #print(F, N, ag, alpha, theta)
    print("The maximum no-slip ramp angle associated with mu_s = 0.65 is", theta * 180 / math.pi, "degrees")

    """
    x = np.linspace(0,1,100)
    y = np.zeros_like(x)


    for i in range(len(x)):
        (F, N, ag, alpha, theta) = fsolve(RollEquations, (0, 0, 0, 0, 0), args=[30,32.2,1.5,0.8,i/len(x)])
        y[i] = theta * 180 / math.pi

    plt.plot(x,y)
    plt.show()
    """

main()

