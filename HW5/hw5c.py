"""
Luke Vaughan
3-2-20
HW5
"""

# Imports necessary libraries
from scipy.integrate import odeint
import matplotlib.pyplot as plt
import numpy as np
import math

# Returns the derivatives used for odeint()
def ode_system(X, t, m, k, c, kf, kb, La, R, v):
    # Names the variables as the state variables
    i = X[0]
    x = X[1]
    xdot = X[2]

    # Calculates the derivatives
    idot = (v - kb * xdot - R*i)/(La)
    xddot = (-c*xdot - k*x + kf*i)/m

    # Returns the value fo the derivatives
    return [idot, xdot, xddot]

def main():
    # Creates and initializes the time array
    t = np.linspace(0,0.015,5000)

    # Sets the initial conditions
    ic = [0, 0, 0]

    # The values for the constant parameters
    m = 0.002
    k = 8*10**5
    c = 2
    kf = 16
    kb = kf
    La = 2*10**(-3)
    R = 12

    # The value of the input voltage
    v = 10

    # Uses odeint() to get the solution for current and position over time
    x1 = odeint(ode_system, ic, t, args=(m, k, c, kf, kb, La, R, v))

    # Plots the solution for x(t) with nice title and labels
    plt.plot(t, x1[:,1])
    plt.title('Position  vs Time')
    plt.xlabel('Time, s')
    plt.ylabel('Position, m')
    plt.show()

    # Plots the solution for i(t) with nice title and labels
    plt.plot(t, x1[:,0])
    plt.title('Current  vs Time')
    plt.xlabel('Time, s')
    plt.ylabel('Current, A')
    plt.show()

    # Changes the value of c to 2.5 and plots the solution on the same graph of c = 25
    c = 2.5
    x2 = odeint(ode_system, ic, t, args=(m, k, c, kf, kb, La, R, v))
    plt.plot(t, x2[:, 1], 'b')
    plt.title('Position  vs Time')
    plt.xlabel('Time, s')
    plt.ylabel('Position, m')

    # Change the value of c to 25 and plot the solution on the same graph of c = 2.5
    c = 25
    x3 = odeint(ode_system, ic, t, args=(m, k, c, kf, kb, La, R, v))
    plt.plot(t, x3[:, 1], 'r')
    plt.title('Position  vs Time')
    plt.xlabel('Time, s')
    plt.ylabel('Position, m')

    # Creates a legend and shows the graph
    plt.legend(('Position for c = 2.5','Position for c = 25'))
    plt.show()

    # Generates the position as a function of time for the suggested solution
    x4 = np.zeros_like(t)
    for i in range(len(t)):
        x4[i] = (1.67-0.3*math.exp(-500*t[i])*math.sin(20000*t[i]+1.55))/(10**5)

    # Plots the suggested solution and the generated solution for x(t)
    plt.plot(t,x4, 'b')
    plt.plot(t, x1[:, 1], 'r')
    plt.title('Position  vs Time')
    plt.xlabel('Time, s')
    plt.ylabel('Position, m')
    plt.legend(('Suggested Solution', 'Differential Equation Solution'))
    plt.show()

main()