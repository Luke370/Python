# Standard GUI Imports
import sys
from PyQt5.QtWidgets import QDialog, QApplication
from PyQt5.QtWidgets import QFileDialog, QMessageBox
from PyQt5.QtGui import QCursor
from PyQt5.QtCore import Qt

# Standard Plot Imports
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

# Other Imports
import numpy as np
from CubicSpline import CubicSpline


# ******************************************************************


# Standard Class definition for plotting
class PlotCanvas(FigureCanvas):
    def __init__(self, parent, width=None, height=None, dpi=100):
        if width == None: width = parent.width() / 100
        if height == None: height = parent.height() / 100
        fig = Figure(figsize=(width, height), dpi=dpi)
        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

    def plotit(self, x, y, slope1, slope2, title, showpoints = True, npoints = 500):

        self.figure.clf()

        coeff = CubicSpline(x, y, slope1, slope2)

        N = len(x)
        u = np.linspace(x[0], x[N - 1], npoints)
        v = np.zeros((len(u)))

        m = 0
        for i in range(len(u)):
            for j in range(len(coeff[0, :])):
                if u[i] >= x[m + 1] and m < len(x) - 2:
                    m += 1
                v[i] += coeff[m, j] * (u[i] - x[m]) ** j


        ax = self.figure.add_subplot(111)
        if showpoints == True:
            ax.plot(x, y, 'bo')
        ax.plot(u, v)
        ax.set_title(title)
        self.draw()

# Standard GUI Import
from CubicSplineGUI import Ui_Dialog

class main_window(QDialog):
    def __init__(self):
        # Standard Declarations
        super(main_window, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.assign_widgets()
        plotwin = self.ui.graphicsView
        self.m = PlotCanvas(plotwin)
        self.show()
        # My Declarations
        self.filename = None
        self.title = None
        self.slope1 = None
        self.slope2 = None
        self.xdata = []
        self.ydata = []

    def assign_widgets(self):
        self.ui.pushButton_Exit.clicked.connect(self.ExitApp)
        self.ui.pushButton_Calc.clicked.connect(self.LoadandCalculate)


    def ReadFileData(self, data):
        self.xdata = []
        self.ydata = []
        for line in data:
            cells = line.strip().split(',')
            keyword = cells[0].lower()
            if keyword == 'title':
                self.title = cells[1]
            if keyword == 'slope1':
                self.slope1 = cells[1]
            if keyword == 'slope2':
                self.slope2 = cells[1]
            if keyword == 'xdata':
                for i in range(1, len(cells)):
                    try:
                        self.xdata.append(float(cells[i]))
                    except:
                        pass
            if keyword == 'ydata':
                for i in cells:
                    try:
                        self.ydata.append(float(i))
                    except:
                        pass

    def LoadandCalculate(self):
        # get filename
        self.filename = QFileDialog.getOpenFileName()[0]
        if len(self.filename) == 0:
            no_file()
            return

        self.ui.Filename_Box.setText(self.filename)

        app.processEvents()

        # read the file
        f = open(self.filename, 'r')
        data = f.readlines()
        f.close()

        self.ReadFileData(data)

        self.ui.FirstSlope_Box.setText(self.slope1)
        self.ui.LastSlope_Box.setText(self.slope2)

        n = len(self.xdata)

        """
        # Check which radio box is checked and assign slope value
        if self.ui.RB_SpecifiedSlopes.isChecked():
            self.slope1 = float(self.ui.FirstSlope_Box.text())
            self.slope2 = float(self.ui.LastSlope_Box.text())
        elif self.ui.RB_ZeroSlopes.isChecked():
            self.slope1 = 0
            self.slope2 = 0
        elif self.ui.RB_StandardSlopes.isChecked():
            self.slope1 = (y[1] - y[0]) / (x[1] - x[0])
            self.slope2 = (y[n-1] - y[n-2]) / (x[n-1] - x[n-2])
        """

        coeffs = CubicSpline(self.xdata, self.ydata, self.slope1, self.slope2)

        c = coeffs[0]
        firsteq = '{:.4f} + {:.4f}*(x-{:.4f}) + {:.4f}*(x-{:.4f})^2 + {:.4f}*(x-{:.4f})^3' \
            .format(c[0], c[1], self.xdata[0], c[2], self.xdata[0], c[3], self.xdata[0])

        c = coeffs[n-2]
        lasteq = '{:.4f} + {:.4f}*(x-{:.4f}) + {:.4f}*(x-{:.4f})^2 + {:.4f}*(x-{:.4f})^3' \
            .format(c[0], c[1], self.xdata[n-2], c[2], self.xdata[n-2], c[3], self.xdata[n-2])

        self.ui.FirstEquation.setText(firsteq)
        self.ui.LastEquation.setText(lasteq)
        self.m.plotit(self.xdata, self.ydata, self.slope1, self.slope2, self.title)
        return


    def ExitApp(self):
        app.exit()

# When the no file is called, it prints a statement saying that there is no file selected
def no_file():
    msg = QMessageBox()
    msg.setText('There was no file selected')
    msg.setWindowTitle("No File")
    retval = msg.exec_()
    return None


# If the statements are not able to read the data correctly from the text file, it will output bad file
def bad_file():
    msg = QMessageBox()
    msg.setText('Unable to process the selected file')
    msg.setWindowTitle("Bad File")
    retval = msg.exec_()
    return None

if __name__ == "__main__":
    app = QApplication.instance()
    if not app:
        app = QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater)
    main_win = main_window()
    sys.exit(app.exec_())