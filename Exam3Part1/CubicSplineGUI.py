# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'CubicSplineGUI.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(719, 656)
        self.pushButton_Exit = QtWidgets.QPushButton(Dialog)
        self.pushButton_Exit.setGeometry(QtCore.QRect(600, 90, 93, 28))
        self.pushButton_Exit.setObjectName("pushButton_Exit")
        self.graphicsView = QtWidgets.QGraphicsView(Dialog)
        self.graphicsView.setGeometry(QtCore.QRect(10, 310, 701, 341))
        self.graphicsView.setObjectName("graphicsView")
        self.groupBox_3 = QtWidgets.QGroupBox(Dialog)
        self.groupBox_3.setGeometry(QtCore.QRect(10, 0, 571, 211))
        self.groupBox_3.setObjectName("groupBox_3")
        self.groupBox = QtWidgets.QGroupBox(self.groupBox_3)
        self.groupBox.setGeometry(QtCore.QRect(100, 50, 411, 111))
        self.groupBox.setObjectName("groupBox")
        self.LastSlope_Box = QtWidgets.QLineEdit(self.groupBox)
        self.LastSlope_Box.setGeometry(QtCore.QRect(340, 50, 41, 22))
        self.LastSlope_Box.setObjectName("LastSlope_Box")
        self.FirstSlope = QtWidgets.QLabel(self.groupBox)
        self.FirstSlope.setGeometry(QtCore.QRect(214, 10, 101, 20))
        self.FirstSlope.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.FirstSlope.setObjectName("FirstSlope")
        self.FirstSlope_Box = QtWidgets.QLineEdit(self.groupBox)
        self.FirstSlope_Box.setGeometry(QtCore.QRect(340, 10, 41, 22))
        self.FirstSlope_Box.setObjectName("FirstSlope_Box")
        self.LastSlope = QtWidgets.QLabel(self.groupBox)
        self.LastSlope.setGeometry(QtCore.QRect(204, 50, 111, 20))
        self.LastSlope.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.LastSlope.setObjectName("LastSlope")
        self.RB_SpecifiedSlopes = QtWidgets.QRadioButton(self.groupBox)
        self.RB_SpecifiedSlopes.setGeometry(QtCore.QRect(20, 20, 171, 17))
        self.RB_SpecifiedSlopes.setObjectName("RB_SpecifiedSlopes")
        self.RB_ZeroSlopes = QtWidgets.QRadioButton(self.groupBox)
        self.RB_ZeroSlopes.setGeometry(QtCore.QRect(20, 50, 171, 17))
        self.RB_ZeroSlopes.setObjectName("RB_ZeroSlopes")
        self.RB_StandardSlopes = QtWidgets.QRadioButton(self.groupBox)
        self.RB_StandardSlopes.setGeometry(QtCore.QRect(20, 80, 171, 17))
        self.RB_StandardSlopes.setObjectName("RB_StandardSlopes")
        self.pushButton_Calc = QtWidgets.QPushButton(self.groupBox_3)
        self.pushButton_Calc.setGeometry(QtCore.QRect(230, 170, 151, 28))
        self.pushButton_Calc.setDefault(True)
        self.pushButton_Calc.setObjectName("pushButton_Calc")
        self.Filename = QtWidgets.QLabel(self.groupBox_3)
        self.Filename.setGeometry(QtCore.QRect(10, 20, 81, 20))
        self.Filename.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.Filename.setObjectName("Filename")
        self.Filename_Box = QtWidgets.QLineEdit(self.groupBox_3)
        self.Filename_Box.setGeometry(QtCore.QRect(100, 20, 411, 22))
        self.Filename_Box.setText("")
        self.Filename_Box.setObjectName("Filename_Box")
        self.groupBox_2 = QtWidgets.QGroupBox(Dialog)
        self.groupBox_2.setGeometry(QtCore.QRect(10, 210, 691, 91))
        self.groupBox_2.setObjectName("groupBox_2")
        self.label_3 = QtWidgets.QLabel(self.groupBox_2)
        self.label_3.setGeometry(QtCore.QRect(0, 20, 141, 16))
        self.label_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName("label_3")
        self.FirstEquation = QtWidgets.QLineEdit(self.groupBox_2)
        self.FirstEquation.setGeometry(QtCore.QRect(150, 20, 521, 22))
        self.FirstEquation.setText("")
        self.FirstEquation.setObjectName("FirstEquation")
        self.label_5 = QtWidgets.QLabel(self.groupBox_2)
        self.label_5.setGeometry(QtCore.QRect(0, 50, 141, 16))
        self.label_5.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_5.setObjectName("label_5")
        self.LastEquation = QtWidgets.QLineEdit(self.groupBox_2)
        self.LastEquation.setGeometry(QtCore.QRect(150, 50, 521, 22))
        self.LastEquation.setText("")
        self.LastEquation.setObjectName("LastEquation")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Cubic Spline"))
        self.pushButton_Exit.setText(_translate("Dialog", "Exit"))
        self.groupBox_3.setTitle(_translate("Dialog", "Input"))
        self.groupBox.setTitle(_translate("Dialog", "End of Slopes"))
        self.LastSlope_Box.setText(_translate("Dialog", "0"))
        self.FirstSlope.setText(_translate("Dialog", "Slope at First Point"))
        self.FirstSlope_Box.setText(_translate("Dialog", "0"))
        self.LastSlope.setText(_translate("Dialog", "Slope at Last Point"))
        self.RB_SpecifiedSlopes.setText(_translate("Dialog", "Use Specified Slopes"))
        self.RB_ZeroSlopes.setText(_translate("Dialog", "Use Zero Slopes"))
        self.RB_StandardSlopes.setText(_translate("Dialog", "Use Standard Slopes"))
        self.pushButton_Calc.setText(_translate("Dialog", "Load and Calculate"))
        self.Filename.setText(_translate("Dialog", "Filename:"))
        self.groupBox_2.setTitle(_translate("Dialog", "Output"))
        self.label_3.setText(_translate("Dialog", "First Segment Equation:"))
        self.label_5.setText(_translate("Dialog", "Last Segment Equation:"))

