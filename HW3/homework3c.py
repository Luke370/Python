"""
Luke Vaughan
2-10-20
HW3
"""
"""
Write a python program that includes and calls the following functions:

def STO(thrust):
    Thrust: the value of the engine thrust. Important, this is the only argument to be passed to this
    function. All of the other required airplane parameters may be assumed constant.
    Local variables for those parameters may be defined and assigned values inside the
    STO function. Use the airplane parameter values given in the figure above.
    
    The function calculates and returns: the airplane take-off distance, calculated using the five
    equations given above. The first four of those equations calculate the value of three
    constants to be used in the fifth equation (VTO, A and B). That fifth equation requires the
    use of numerical integration. Use your Simpson function to perform the integration.
    
def ThrustNeededForTakeoff(distance):
    distance: the required take-off distance. Important, this is the only argument to be passed to this
    function. All of the other required airplane parameters may be assumed constant.
    Local variables for those parameters may be defined and assigned values inside the
    ThrustNeededForTakeoff function. Use the airplane parameter values given in the
    figure on the previous page.
    
    The function calculates and returns: the engine thrust needed to allow the airplane to take-off in
    the specified distance. Hint: The ThrustNeededForTakeoff function behaves as the
    inverse of the STO function. Therefore it must use a root-finding method to find the
    value of thrust that causes: STO(thrust) – distance = 0 Use your Secant
    function so solve for this value of thrust.
    
def main():
    main() has no arguments and no return value.
    main() does the following:
    a) calls STO(13000) to calculate the take-off distance for an engine thrust of 13000 pounds.
    Print the answer with one decimal place, using a nice text label. The nice text label should
    include the thrust value of 13000 pounds.
    b) calls ThrustNeededForTakeoff(1500) to calculate the thrust needed to allow takeoff in
    1500 feet. Print the answer with two decimal places, using a nice text label. The nice text
    label should include the takeoff distance of 1500 feet.
    c) calls ThrustNeededForTakeoff(1000) to calculate the thrust needed to allow takeoff in
    1000 feet. Print the answer with two decimal places, using a nice text label. The nice text
    label should include the takeoff distance of 1000 feet.
"""

import math

def STO(thrust):
    weight = 56000
    S = 1000
    Clmax = 2.4
    Cd = 0.0279
    rho = 0.002377
    gc = 32.2

    Vstall = math.sqrt(weight/(0.5*rho*S*Clmax))
    VTO = 1.2 * Vstall
    A = gc * (thrust/weight)
    B = gc / weight * (0.5*rho*S*Cd)

    func = lambda V: V / (A - B*V**2)

    STO = Simpson(func, 0, VTO, 1000)

    return STO

def ThrustNeededForTakeoff(distance):
    def func(thrust):
        return STO(thrust) - distance
    return Secant(func, 10, 20000, 1000, 0.001)

def Simpson(fnc, a, b, npoints):
    # Declarations
    Simp = 0
    divisions = 0
    value = 0

    # Checks if the number of points is odd, and if its isn't it adds an extra point
    if npoints%2 == 0:
        divisions = npoints + 1
    else:
        divisions = npoints

    # Creates the spacing between the rectangles
    deltax = (b-a)/(divisions-1)
    # Initializes x
    x = a

    # Loops through the evenly spaced x values and performs simpson's rule
    for i in range(divisions):
        # For the first end point and the last end point the function is multiplied by 1
        if i == 0 or i == divisions-1:
            Simp += fnc(x)
            x += deltax
        # For the 2nd,4th,6th... x values the function will be multiplied by 4
        elif i%2 == 1:
            Simp += 4*fnc(x)
            x += deltax
        # For the 3rd,5th,7th... x values the function will be multiplied by 2
        else:
            Simp += 2*fnc(x)
            x += deltax
    # Our result of simpson's rule is the sum of all the function values which has been stored in Simp and then needs
    # to be multiplied by deltax/3
    Simp = deltax/3 * Simp
    # Returns the value of Simp and the number of subdivisions
    return Simp

def Secant(fcn, x0, x1, maxiter, xtol):
    iter = 0
    xn = x0
    xnminus1 = x1
    xnplus1 = 0

    while abs(xn - xnminus1) > xtol and iter <= maxiter:
        xnplus1 = xn - fcn(xn) * (xn - xnminus1) / (fcn(xn) - fcn(xnminus1))
        xnminus1 = xn
        xn = xnplus1
        iter += 1
    return xn

def main():
    takeoff = STO(13000)
    print("The value of the takeoff distance for an engine thrust of 13000 pounds is {:.1f} ft".format(takeoff))
    thrust = ThrustNeededForTakeoff(1500)
    print("The value of thrust needed to allow for takeoff distance of 1500 ft is {:.2f} lbs".format(thrust))
    thrust = ThrustNeededForTakeoff(1000)
    print("The value of thrust needed to allow for takeoff distance of 1000 ft is {:.2f} lbs".format(thrust))

main()