"""
Luke Vaughan
2-10-20
HW3
"""
"""
Write a Python Program that does the following:

a) Defines a function that meets the following specifications:
    defColumnWithLargestSum(matrix):
    matrix: full rectangular list of lists of numbers (we usually call this a matrix). Rectangular means that the number
    of rows and the number of columns are not necessarily equal.
    
    The function returns: colnumber, colsum: colnumber is the index number of the column in the matrix whose sum is the
    largest. colsum is the sum of the values in that column.  

b) Contains and calls a main() function that does the following:
    Creates the two matrices shown below, sends Amatrix to ColumnWithLargestSum() and then prints the answer, then sends
    Bmatrix to ColumnWithLargestSum() and then prints the answer.  
    
    Amatrix = [[3, 2, 5, 2, 3],[2, -1, 4, 5, 1],[1, 8, 3, 1.3, 2]]
    Bmartix = [[2, 1, 4],[3, 2, 5],[4, 2, 1],[1, 5, 2],[3, 1, 1]]
"""

def ColumnWithLargestSum(matrix):
    largestvalue = -9999
    largestcol = -1

    for i in range(len(matrix[0])):
        currentsum = 0
        currentcol = i
        for j in range(len(matrix)):
            currentsum += matrix[j][i]
        if currentsum > largestvalue:
            largestvalue = currentsum
            largestcol = currentcol
    return largestvalue, largestcol + 1

def main():
    Amatrix = [[3, 2, 5, 2, 3], [2, -1, 4, 5, 1], [1, 8, 3, 1.3, 2]]
    Bmatrix = [[2, 1, 4], [3, 2, 5], [4, 2, 1], [1, 5, 2], [3, 1, 1]]
    value, column = ColumnWithLargestSum(Amatrix)
    print("The largest sum in is column", column,"and has a value of", value)
    value, column = ColumnWithLargestSum(Bmatrix)
    print("The largest sum in is column", column, "and has a value of", value)

main()