"""
Luke Vaughan
2-10-20
HW3
"""
"""
Write a Python program that does the following:

a) Defines a function that meets the following  specifications:
    defMagVectorDiff(vector1, vector2):
    vector1 and vector2: are both lists of numbers, representing vectors. They are vectors of the same size!
    
    The function returns: a single numerical value, containing the magnitude of the vector difference between vector1
     and vector2.
    
b) Contains a call a main() function that does the following:
    Creates the four vectors shown below, sends A and B to MagVectorDiff() and then prints the answer, then sends C and 
    D to MagVectorDiff() and then prints the answer.
    
    A = [2, 1, 5, 9]
    B = [1, 2, 7, 5]
    C = [1.5, 3, 4, 7, 3]
    D = [2, 4.2, 4, 6, 3]
"""
import math

def MagVectorDiff(vector1, vector2):
    pythag = 0
    answer = [0] * len(vector1)
    for i in range(len(vector1)):
        answer[i] = vector1[i]-vector2[i]
        pythag += answer[i]**2
    return math.sqrt(pythag)

def main():
    A = [2, 1, 5, 9]
    B = [1, 2, 7, 5]
    C = [1.5, 3, 4, 7, 3]
    D = [2, 4.2, 4, 6, 3]
    answer = MagVectorDiff(A,B)
    print("The magnitude of the difference between A and B is", answer)
    answer = MagVectorDiff(C, D)
    print("The magnitude of the difference between C and D is", answer)

main()