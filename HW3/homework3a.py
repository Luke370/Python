"""
Luke Vaughan
2-10-20
HW3
"""
"""
Write a Python program that does the following:

a) Defines a function that meets the following specifications: 

def MatrixTermProducts(Amatrix,Bmatrix): Amatrix and Bmatrix: two matrices (list of lists) of integers, rectangular in 
    shape (but not necessarily square) with no missing terms. They are identical in size. 

The function returns: a new independent matrix, exactly the same size as Amatrix and Bmatrix, where the value at each 
    location in this new matrix contains the product of the two values at that same location in Amatrix and Bmatrix. 
"""

def MatrixTermProducts(Amatrix, Bmatrix):
    # Initialize a zero matrix the same size as Amatrix and Bmatrix
    Cmatrix = [0] * len(Amatrix)
    for i in range(len(Amatrix)):
        Cmatrix[i] = [0] * len(Amatrix[0])
    # Iterates through each index and stores their product in a new matrix
    for i in range(len(Amatrix)):
        for j in range(len(Amatrix[i])):
             Cmatrix[i][j] = float(Amatrix[i][j]) * float(Bmatrix[i][j])
    # returns the new matrix
    return Cmatrix

def main():
    matrix1=[[1, -5, 3, -7],[2, 5, 11, -2], [13, -1, 0, 4]]
    matrix2=[[5, 0, 1, 4],[4, 3, 2, 1],[1, 2, 3, 4]]
    answer = MatrixTermProducts(matrix1, matrix2)
    print(answer)

main()