"""
Luke Vaughan
2-10-20
HW3
"""
"""
Write a Python program that defines and calls the following three functions:
    a) def SigmaMax(z)
        z: the wing section modulus
        
        The function calculates and returns: the maximum bending stress at the root of the wing, calculated using the
        given value of z and the procedure shown on the previous page. It uses the lift force function and the wing
        length given at the bottom of the previous page. You must use your Simpson function to perform the required
        integration.
        
    b) def DesignTheSpar(DesignStress)
        DesignStress: the maximum allowable (safe) wing stress.
        
        The function claculates and returns: the section modulus (z) required for a wing to safely survive the wing
        loading given on the previous page.
        
    c) def main():
        a) calls SigmaMax ( 3.5) to calculate the stress for a value of z = 3.5. Print the value of z and
        the value of wing stress, each formatted with one decimal place, using nice text labeling.
    
        b) calls SigmaMax ( 1.5) to calculate the stress for a value of z = 1.5. Print the value of z and
        the value of wing stress, each formatted with one decimal place, using nice text labeling.
        
        c) calls DesignTheSpar (25000) to calculate the section modulus needed if the design stress
        is 25000. Print the value of Design Stress and the value of wing section modulus, each
        formatted with two decimal place, using nice text labeling. 
"""

import math

def SigmaMax(z):
    length = 320
    integrand = lambda x: x * 1.5 * math.cos(x/length)
    Mroot = Simpson(integrand, 0, length, 10000)
    return Mroot/z

def DesignTheSpar(DesignStress):
    func = lambda z: SigmaMax(z) - DesignStress
    return Secant(func, 1, 5, 10000, 0.001)

def Simpson(fnc, a, b, npoints):
    # Declarations
    Simp = 0
    divisions = 0
    value = 0

    # Checks if the number of points is odd, and if its isn't it adds an extra point
    if npoints%2 == 0:
        divisions = npoints + 1
    else:
        divisions = npoints

    # Creates the spacing between the rectangles
    deltax = (b-a)/(divisions-1)
    # Initializes x
    x = a

    # Loops through the evenly spaced x values and performs simpson's rule
    for i in range(divisions):
        # For the first end point and the last end point the function is multiplied by 1
        if i == 0 or i == divisions-1:
            Simp += fnc(x)
            x += deltax
        # For the 2nd,4th,6th... x values the function will be multiplied by 4
        elif i%2 == 1:
            Simp += 4*fnc(x)
            x += deltax
        # For the 3rd,5th,7th... x values the function will be multiplied by 2
        else:
            Simp += 2*fnc(x)
            x += deltax
    # Our result of simpson's rule is the sum of all the function values which has been stored in Simp and then needs
    # to be multiplied by deltax/3
    Simp = deltax/3 * Simp
    # Returns the value of Simp and the number of subdivisions
    return Simp

def Secant(fcn, x0, x1, maxiter, xtol):
    iter = 0
    xn = x0
    xnminus1 = x1
    xnplus1 = 0

    while abs(xn - xnminus1) > xtol and iter <= maxiter:
        xnplus1 = xn - fcn(xn) * (xn - xnminus1) / (fcn(xn) - fcn(xnminus1))
        xnminus1 = xn
        xn = xnplus1
        iter += 1
    return xn

def main():
    z = 3.5
    stress = SigmaMax(z)
    print("For a z value of {:.1f}, the value of wing stress is {:.1f}".format(z, stress))
    z = 1.5
    stress = SigmaMax(z)
    print("For a z value of {:.1f}, the value of wing stress is {:.1f}".format(z, stress))
    stress = 25000
    z = DesignTheSpar(stress)
    print("For a stress of {:.2f}, the value of wing section modulus is {:.2f}".format(stress, z))

main()