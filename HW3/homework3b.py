"""
Luke Vaughan
2-10-20
HW3
"""
"""
Write a python program that does the following:

a) defines a fucntion that meets the following specifications:
    def LookingForSum(thisarray, sumval):
    thisarray: an array (list) of integers
    sumval: an integer
    
    The function returns: an integer specifying the first location in the list where the sum of a sequence of terms add 
    up to sumval. If there is no such location, return -1.
"""

def LookingForSum(thisarray, sumval):
    position = 0
    for i in range(len(thisarray)):
        sum = 0
        j = 0
        position = i
        while sum != sumval:
            sum += thisarray[i+j]
            if j < len(thisarray)- i - 1:
                j += 1
            else:
                break
        if sum == sumval:
            return position
    return -1

def main():
    thatarray= [3, 4, 2, -7, 5, 2, 1, -1]
    val1 = LookingForSum(thatarray, -2)
    print("val1 = ", val1)
    val2 = LookingForSum(thatarray, 8)
    print("val2 = ", val2)

main()