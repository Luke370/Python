# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'DuctGUI.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(703, 456)
        self.groupBox = QtWidgets.QGroupBox(Dialog)
        self.groupBox.setGeometry(QtCore.QRect(10, 10, 591, 111))
        self.groupBox.setObjectName("groupBox")
        self.Load = QtWidgets.QPushButton(self.groupBox)
        self.Load.setGeometry(QtCore.QRect(20, 30, 151, 23))
        self.Load.setObjectName("Load")
        self.filelocation = QtWidgets.QTextEdit(self.groupBox)
        self.filelocation.setGeometry(QtCore.QRect(190, 20, 381, 81))
        self.filelocation.setObjectName("filelocation")
        self.groupBox_2 = QtWidgets.QGroupBox(Dialog)
        self.groupBox_2.setGeometry(QtCore.QRect(10, 130, 671, 281))
        self.groupBox_2.setObjectName("groupBox_2")
        self.groupBox_3 = QtWidgets.QGroupBox(self.groupBox_2)
        self.groupBox_3.setGeometry(QtCore.QRect(499, 20, 161, 171))
        self.groupBox_3.setObjectName("groupBox_3")
        self.label = QtWidgets.QLabel(self.groupBox_3)
        self.label.setGeometry(QtCore.QRect(10, 20, 51, 16))
        self.label.setObjectName("label")
        self.Dif_ID = QtWidgets.QLineEdit(self.groupBox_3)
        self.Dif_ID.setGeometry(QtCore.QRect(80, 20, 71, 20))
        self.Dif_ID.setReadOnly(True)
        self.Dif_ID.setObjectName("Dif_ID")
        self.label_2 = QtWidgets.QLabel(self.groupBox_3)
        self.label_2.setGeometry(QtCore.QRect(10, 60, 61, 16))
        self.label_2.setObjectName("label_2")
        self.Run = QtWidgets.QLineEdit(self.groupBox_3)
        self.Run.setGeometry(QtCore.QRect(80, 60, 71, 20))
        self.Run.setReadOnly(True)
        self.Run.setObjectName("Run")
        self.label_3 = QtWidgets.QLabel(self.groupBox_3)
        self.label_3.setGeometry(QtCore.QRect(30, 90, 91, 16))
        self.label_3.setObjectName("label_3")
        self.longrun = QtWidgets.QTextEdit(self.groupBox_3)
        self.longrun.setGeometry(QtCore.QRect(10, 110, 141, 51))
        self.longrun.setObjectName("longrun")
        self.label_4 = QtWidgets.QLabel(self.groupBox_2)
        self.label_4.setGeometry(QtCore.QRect(500, 190, 151, 21))
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.groupBox_2)
        self.label_5.setGeometry(QtCore.QRect(500, 210, 151, 21))
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.groupBox_2)
        self.label_6.setGeometry(QtCore.QRect(500, 230, 151, 21))
        self.label_6.setObjectName("label_6")
        self.label_7 = QtWidgets.QLabel(self.groupBox_2)
        self.label_7.setGeometry(QtCore.QRect(500, 250, 151, 21))
        self.label_7.setObjectName("label_7")
        self.report = QtWidgets.QTextBrowser(self.groupBox_2)
        self.report.setGeometry(QtCore.QRect(10, 20, 471, 251))
        self.report.setObjectName("report")
        self.Exit = QtWidgets.QPushButton(Dialog)
        self.Exit.setGeometry(QtCore.QRect(280, 420, 75, 23))
        self.Exit.setObjectName("Exit")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Duct Design"))
        self.groupBox.setTitle(_translate("Dialog", "Duct File"))
        self.Load.setText(_translate("Dialog", "Open and Read a Duct File"))
        self.groupBox_2.setTitle(_translate("Dialog", "Duct Report"))
        self.groupBox_3.setTitle(_translate("Dialog", "Longest Length Run"))
        self.label.setText(_translate("Dialog", "Diffuser ID"))
        self.label_2.setText(_translate("Dialog", "Run Length"))
        self.label_3.setText(_translate("Dialog", "Longest Run Path"))
        self.label_4.setText(_translate("Dialog", "Contributors to this program:"))
        self.label_5.setText(_translate("Dialog", "Dade Eddy"))
        self.label_6.setText(_translate("Dialog", "Brenden Dominick"))
        self.label_7.setText(_translate("Dialog", "Luke Vaughan"))
        self.Exit.setText(_translate("Dialog", "Exit"))

