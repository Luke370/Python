# Defines the Fitting Class
class Fitting:

    # Initializes variables of the Fitting Class
    def __init__(self):
        self.ID = None
        self.type = None
        self.upFittingID = None
        self.upFitting = None
        self.data = None
        self.length = None
        self.flow = None

# Defines the Duct Class
class Duct:

    # Initializes variables of the Duct Class
    def __init__(self):
        self.title = None
        self.fanPressure = None
        self.airDensity = None
        self.roughness = None
        self.rounding = None
        self.fittings = []  # An empty list of links
        self.longestRunVal = -99999999
        self.longestRunDiffuser = None
        self.longestRunPath = None

    # Reads the data from a pre-organized text file.
    def ReadDuctData(self, data):
        #  Data is an array of strings, read from the duct design data file
        for line in data:                                    # Loop over all the lines
            cells = line.strip().split(',')                  # Splits the cells by commas and strips whitespace
            keyword = cells[0].lower()                       # Saves the first item in cells in lowercase as keyword
            if keyword == 'title':                           # Stores the name of the title
                self.title = cells[1].replace("'", "")
            if keyword == 'fan_pressure':                    # Stores the value of fan pressure
                self.fanPressure = float(cells[1])
            if keyword == 'air_density':                     # Stores the value of air density
                self.airDensity = float(cells[1])
            if keyword == 'roughness':                       # Stores the value of roughness
                self.roughness = float(cells[1])
            if keyword == 'rounding':                        # Stores the value of rounding
                self.rounding = cells[1]
            if keyword == 'fitting':                         # Breaks down the values of fittings
                thisfitting = Fitting()
                thisfitting.ID = cells[1].strip()            # Stores the ID of the Fitting
                thisfitting.type = cells[2].strip()          # Stores the type of the fitting
                try:
                    thisfitting.upFittingID = cells[3].strip()      # Stores upstream fitting ID number
                    thisfitting.data = float(cells[4].strip())      # Stores data of the fitting
                    if thisfitting.type.lower() == 'duct':
                        thisfitting.length = float(cells[4].strip())    # Stores the length of ducts
                    elif thisfitting.type.lower() == 'diffuser':
                        thisfitting.flow = float(cells[4].strip())      # Stores the flow rate of diffusers
                    self.fittings.append(thisfitting)               # Creates  a list of all fittings
                except:
                    self.fittings.append(thisfitting)               # Creates a list of all fittings (including tees)
                    continue
        self.UpdateConnections()

    # Updates the connections
    def UpdateConnections(self):
        for fitting in self.fittings:                           # Loops through fittings
            if fitting.ID == 'beginning':                       # Skips of beginning
                continue
            if '-' in fitting.upFittingID:                      # Checks for hyphen in the ID
                name = fitting.upFittingID.split('-')           # stores the name in a list
                fitting.upFittingID = name[0]                   # Updates the value of the ID
                fitting.upFitting = FindingFittingsByID(fitting.upFittingID, self.fittings)     # Finds upstream fitting
            else:
                fitting.upFitting = FindingFittingsByID(fitting.upFittingID, self.fittings)     # Finds upstream fitting

    # Finds the longest run of duct from a diffuser to the AHU
    def FindLongestRun(self):
        # get fitting lengths
        self.longestRunVal = -99999999
        self.longestRunDiffuser = None
        for fitting in self.fittings:                       # Loops through fittings
            if fitting.type.lower() == 'diffuser':          # If fitting is diffuser find the path to fan
                path, distance = self.PathToFan(fitting)    # Finds the path an the distance
                if distance > self.longestRunVal:           # Compares to the longest run value
                    self.longestRunVal = distance           # Stores the longest run value
                    self.longestRunDiffuser = fitting.ID    # Stores the Diffuser ID of the longest run
                    self.longestRunPath = ', '.join(path)   # Formats the path into one organized string

    # Finds the path to the fan
    def PathToFan(self, fittings):
        path = []                                       # Initializes a blank path
        distance = 0                                    # Initializes distance
        path.append(fittings.ID)                        # Appends the current diffuser ID
        if fittings.type.lower() == 'duct':             # Checks to see if fitting is a duct
            distance += fittings.data                   # Adds the length of the duct to the distance
        if fittings.ID == 'beginning':                  # If fitting is beginning then stop recursion
            return path, distance                       # Returns path and distance
        fittings.upFitting = FindingFittingsByID(fittings.upFittingID, self.fittings)       # Finds upstream ID
        p, d = self.PathToFan(fittings.upFitting)       # Uses recursion to find the path
        path += p                                       # Adds all the paths together
        distance += d                                   # Adds all the distances together
        return path, distance                           # Returns path and distance

    # Generates report
    def GenerateReport(self):
        report = '\t                   Duct Design Report'                  # Report Heading
        report += '\n\tTitle: ' + self.title                                # Report title
        report += '\n\tFan Pressure: ' + str(self.fanPressure)              # Prints fan pressure
        report += '\n\tAir Density: ' + str(self.airDensity)                # Prints air density
        report += '\n\tRoughness: ' + str(self.roughness)                   # Prints roughness
        report += '\n\tRounding: ' + self.rounding                          # Prints rounding
        report += '\n'
        report += '\n\t----------Fitting Summary----------------'           # Prints fitting summary
        report += '\n'
        report += '\nFitting\tType\tUpstream Fitting\t\tData'               # Prints column headers
        for fitting in self.fittings:                                       # Loops through fittings and prints data
            if fitting.upFittingID is None:                                 # Prints spaces instead of NONE for AHU
                report += '\n{}\t{}\t{}\t\t{}'.format(fitting.ID, fitting.type, '', '')
            elif fitting.data is None:                                      # Prints spaces instead of NONE for tees
                report += '\n{}\t{}\t{}\t\t{}'.format(fitting.ID, fitting.type, fitting.upFittingID, '')
            else:                                                           # Prints data for diffusers and ducts
                report += '\n{}\t{}\t{}\t\t{}'.format(fitting.ID, fitting.type, fitting.upFittingID, fitting.data)
        return report                                                       # Returns the report as a string


# Finds fitting class by its ID
def FindingFittingsByID(ID, fittinglist):
    # Search a list of objects to find one with a particular name
    # of course, the objects must have a "name" member.
    for fitting in fittinglist:                         # All objects in the list
        if fitting.ID == ID:                            # Does it have the name I am seeking?
            return fitting                              # Then return this one
    # Next item
    return None  # couldn't find it


def main():
    # Read File
    f1 = open('Duct Design Input File 2.txt', 'r')      # Open the file for reading
    data = f1.readlines()                               # Read the entire file as a list of strings
    f1.close()                                          # Close the file
    duct = Duct()                                       # duct is a Duct class
    duct.ReadDuctData(data)                             # Imports the data into the Duct Class
    duct.FindLongestRun()                               # Finds the longest run
    rpt = duct.GenerateReport()                         # Generates report
    print('The longest path is: ', duct.longestRunPath) # Prints the longest path


if __name__ == "__main__":
    main()
