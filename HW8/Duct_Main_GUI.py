# startup functions
import numpy as np
import matplotlib.pyplot as plt

import sys
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QDialog, QApplication
from PyQt5.QtWidgets import QFileDialog, QMessageBox
from PyQt5.QtGui import QCursor
from PyQt5.QtCore import Qt

#  import functions
from DuctGUI import Ui_Dialog
from Duct_Class import Duct


class main_window(QDialog):
    def __init__(self):
        super(main_window, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.assign_widgets()

        # Define the 3 data items
        self.duct = None
        self.filename = None
        self.startupname = None

        self.show()

    def assign_widgets(self):
        # assigns the names of buttons on the application window
        self.ui.Exit.clicked.connect(self.ExitApp)
        self.ui.Load.clicked.connect(self.GetDuct)

    def GetDuct(self):
        # get the file name using the OPEN Dialog
        self.filename = QFileDialog.getOpenFileName()[0]

        # if there is no input, then it gives an error that there is no file, gives popup to user
        if len(self.filename) == 0:
            no_file()
            return
        # Updates the textbox on the screen to showcase the name of the file
        self.ui.filelocation.setText(self.filename)
        app.processEvents()

        # Gives the user a wait cursor to show that the application is actually working
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))

        # Read the file
        f1 = open(self.filename, 'r')  # open the file for reading
        data = f1.readlines()  # read the entire file as a list of strings
        f1.close()  # close the file  ... very important

        self.duct = Duct()  # create a duct instance (object)
        try:
            # sends the duct application the data from the user selected file
            self.duct.ReadDuctData(data)
            self.duct.FindLongestRun()

            # put the report in the large TextBox
            rpt = self.duct.GenerateReport()
            self.ui.report.setText(rpt)

            # fill the small text boxes
            self.ui.Dif_ID.setText(self.duct.longestRunDiffuser)
            self.ui.Run.setText(str(self.duct.longestRunVal))
            self.ui.longrun.setText(self.duct.longestRunPath)

            # Overrides the cursor so it is no longer in the hour glass
            QApplication.restoreOverrideCursor()
        except:  # if the file is not able to conduct all the above statements then it will call the bad_file() function
            QApplication.restoreOverrideCursor()
            bad_file()

    # exits the application when the EXIT Button is selected
    def ExitApp(self):
        app.exit()


# When the no file is called, it prints a statement saying that there is no file selected
def no_file():
    msg = QMessageBox()
    msg.setText('There was no file selected')
    msg.setWindowTitle("No File")
    retval = msg.exec_()
    return None


# If the statements are not able to read the data correctly from the text file, it will output bad file
def bad_file():
    msg = QMessageBox()
    msg.setText('Unable to process the selected file')
    msg.setWindowTitle("Bad File")
    retval = msg.exec_()
    return None


if __name__ == "__main__":
    app = QApplication.instance()
    if not app:
        app = QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater)
    main_win = main_window()

    # if a filename exists at the beginning of the program .. open it
    # ... to save time in the debugging cycle
    name = main_win.filename
    if name is not None:
        main_win.GetTruss(name=main_win.filename)

    sys.exit(app.exec_())
