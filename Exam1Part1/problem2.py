"""
Luke Vaughan
Part 1 Problem 2
"""

def combineonDiagnoal(matrix1, matrix2):
    row = len(matrix1) + len(matrix2)
    column = len(matrix1[0]) + len(matrix2[0])

    matrix3 = [0] * row
    for i in range(row):
        matrix3[i] = [0] * column
    for i in range(len(matrix1)):
        for j in range(len(matrix1[i])):
            matrix3[i][j] = matrix1[i][j]
    for i in range(len(matrix1),row):
        for j in range(len(matrix1[0]),column):
            matrix3[i][j] = matrix2[i-len(matrix1)][j-len(matrix1[0])]
    return matrix3

def main():
    m1 = [[1, 2, 3],[4, 5, 6]]
    m2 = [[5, 4, 3, 2, 1],[3, 4, 5, 6, 7],[9, 8, 7, 6, 5]]
    m3 = [[1, 2],[3, 4],[5, 6]]

    answer = combineonDiagnoal(m1,m2)
    print(answer)
    answer = combineonDiagnoal(m2,m3)
    print(answer)

main()