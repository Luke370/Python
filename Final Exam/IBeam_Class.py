import sys
from PyQt5.QtWidgets import QDialog, QApplication
from PyQt5.QtWidgets import QFileDialog, QMessageBox
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtGui import QCursor
from PyQt5.QtCore import Qt, QEvent
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from OpenGL_2D_class import gl2D, gl2DText, gl2DCircle


class IBeam:
    def __init__(self):
        self.H = None
        self.W = None
        self.thf = None
        self.thw = None
        self.F = None
        self.L = None
        self.ys = None
        self.drawingsize = None

    def ReadData(self, data):
        for line in data:
            cells = line.strip().split(',')
            keyword = cells[0].lower()
            s = self
            if keyword.lower() == 'ibeam':
                s.H = float(cells[1].strip())
                s.W = float(cells[2].strip())
                s.thf = float(cells[3].strip())
                s.thw = float(cells[4].strip())
                s.F = float(cells[5].strip())
                s.L = float(cells[6].strip())

            if keyword.lower() == 'yield':
                s.ys = float(cells[1].strip())

    def DrawBeamPicture(self):
        midP = self.W / 2
        wall = self.thw / 2
        glColor3f(0, 1, 0)
        glLineWidth(1)
        glBegin(GL_LINE_STRIP)
        glVertex2f(0, 0)  # Point 1
        glVertex2f(0, self.thf)  # Point 2
        glVertex2f(midP - wall, self.thf)  # Point 3
        glVertex2f(midP - wall, self.H - self.thf)  # Point 4
        glVertex2f(0, self.H - self.thf)  # Point 5
        glVertex2f(0, self.H)  # point 6
        glVertex2f(self.W, self.H)  # Point 7
        glVertex2f(self.W, self.H - self.thf)  # Point 8
        glVertex2f(midP + wall, self.H - self.thf)  # point 9
        glVertex2f(midP + wall, self.thf)  # Point 10
        glVertex2f(self.W, self.thf)  # Point 11
        glVertex2f(self.W, 0)  # Pooint 12
        glVertex2f(0, 0)  # Point 13
        glEnd()

    def Stresses(self):
        H_inner = self.H - 2 * self.thf

        A_f = self.W * self.thf
        A_w = self.thw * H_inner
        A_total = A_w + 2 * A_f

        I1 = self.thw * H_inner**3 / 12
        I2 = self.W * self.thf**3 / 12
        I3 = I2 + A_f * (H_inner/2 + self.thf/2)**2
        I_total = I1 + 2*I3

        Q = A_w/2 * H_inner/4 + A_f * (H_inner/2 + self.thf/2)
        M = self.F * self.L
        c = self.H / 2
        sigma_top = (M * c) / I_total

        V = self.F
        b = self.thw
        sigma_mid = (1/0.577) * (V * Q) / (I_total * b)

        return sigma_top, sigma_mid, A_total


def main():
    f1 = open('beam1.txt', 'r')
    data = f1.readlines()
    f1.close()

    b = IBeam()
    b.ReadData(data)
    t, m, a = b.Stresses()
    print(round(t, 0), round(m, 0), a)


if __name__ == "__main__":
    main()
