#Dade Edday
#Brenden Dominick
#Luke Vaughn


#startup functions
import numpy as np
import matplotlib.pyplot as plt

import sys
from PyQt5.QtWidgets import QDialog, QApplication
from PyQt5.QtWidgets import QFileDialog, QMessageBox
from PyQt5.QtGui import QCursor
from PyQt5.QtCore import Qt
from PyQt5 import QtCore, QtGui, QtWidgets, uic

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

# Import functions
from Ibeam_ui import Ui_Dialog
from IBeam_Class import *


class main_window(QDialog):
    def __init__(self):
        super(main_window, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.assign_widgets()

        self.setupGLWindows()
        self.beam = IBeam()
        #variables
        self.filename = None
        self.beam = None

        self.topStress = None
        self.middleStress = None
        self.beamArea = None

        self.show()

    def assign_widgets(self):
        self.ui.pushButton_Exit.clicked.connect(self.ExitApp)
        self.ui.pushButton_GetFile.clicked.connect(self.GetFile)
        self.ui.pushButton_minimize.clicked.connect(self.minimize)
        self.ui.pushButton_calculate.clicked.connect(self.getStress)

    def GetFile(self):
        self.filename = QFileDialog.getOpenFileName()[0]

        # if there is no input, then it gives an error that there is no file, gives popup to user
        if len(self.filename) == 0:
            no_file()
            return
        # Updates the textbox on the screen to showcase the name of the file
        self.ui.textEdit_filename.setText(self.filename)
        app.processEvents()


        # Read the file
        f1 = open(self.filename, 'r')  # open the file for reading
        data = f1.readlines()  # read the entire file as a list of strings
        f1.close()  # close the file

        if len(self.filename) == 0:
            no_file()
            return


        self.beam = IBeam()  # Create an instance for Balloon Flight
        self.beam.ReadData(data)


        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        try:

            # Fills the lower boxes with the beginning stages
            b = self.beam
            string = str(b.H)
            self.ui.lineEdit_height.setText(string)

            string = str(b.W)
            self.ui.lineEdit_width.setText(string)

            string = str(b.thf)
            self.ui.lineEdit_thF.setText(string)

            string = str(b.thw)
            self.ui.lineEdit_thW.setText(string)

            string = str(b.L)
            self.ui.lineEdit_length.setText(string)

            string = str(b.F)
            self.ui.lineEdit_force.setText(string)

            string = str(b.ys)
            self.ui.lineEdit_sy.setText(string)

            self.glwindow1.setViewSize(-self.beam.thw, self.beam.W + self.beam.thw, -self.beam.thf,
                                       self.beam.H + self.beam.thf, allowDistortion=False)

            # Overrides the cursor so it is no longer in the hour glass
            QApplication.restoreOverrideCursor()

        except:  # if the file is not able to conduct all the above statements then it will call the bad_file() function
            QApplication.restoreOverrideCursor()
            bad_file()

    def getStress(self):
        height = float(self.ui.lineEdit_height.text())
        width = float(self.ui.lineEdit_width.text())
        flangeth = float(self.ui.lineEdit_thF.text())
        webth = float(self.ui.lineEdit_thW.text())
        length = float(self.ui.lineEdit_length.text())
        force = float(self.ui.lineEdit_force.text())
        yieldst = float(self.ui.lineEdit_sy.text())

        b = self.beam

        b.H = height
        b.W = width
        b.thf = flangeth
        b.thw = webth
        b.F = force
        b.L = length
        b.ys = yieldst

        top, mid, area = b.Stresses()
        self.ui.lineEdit_top.setText(str(round(top, 0)))
        self.ui.lineEdit_mid.setText(str(round(mid, 0)))
        self.ui.lineEdit_area.setText(str(area))

        self.glwindow1.setViewSize(-self.beam.thw, self.beam.W + self.beam.thw, -self.beam.thf,
                                   self.beam.H + self.beam.thf, allowDistortion=False)
        self.glwindow1.glUpdate()

    def setupGLWindows(self):
        self.glwindow1 = gl2D(self.ui.openGLWidget, self.DrawingCallback)
        self.glwindow1.setViewSize(-.5, .5, -.5, .5, allowDistortion=True)

    def DrawingCallback(self):
        # This is what actually draws the picture
        if self.beam is not None:
            self.beam.DrawBeamPicture()

    def ExitApp(self): # closes the application window and ends the process
        app.exit()

    def minimize(self):
        pass


def no_file():
    msg = QMessageBox()
    msg.setText('There was no file selected')
    msg.setWindowTitle("No File")
    retval = msg.exec_()
    return None


def bad_file(): #If the statements are not able to read the data correctly from the text file, it will output bad file
    msg = QMessageBox()
    msg.setText('Unable to process the selected file')
    msg.setWindowTitle("Bad File")
    retval = msg.exec_()
    return None


if __name__ == "__main__":
    app = QApplication.instance()
    if not app:
        app = QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater)
    main_win = main_window()
    sys.exit(app.exec_())