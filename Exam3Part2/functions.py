import math
from scipy.optimize import fsolve

def CLa(AR):
    return (math.pi * AR) / (1 + math.sqrt(1 + (AR / 2) ** 2))

def epsilon(e, AR):
    return (1 / (math.pi * e * AR))

def CL(CLa, Alpha):
    return CLa * Alpha

def CD(CDo, epsilon, CL):
    return CDo + epsilon * CL ** 2

def equations(vals, Alpha, S, AR, e, m, CDo, g, rho):
    (V, Gam) = vals

    f1 = -1 * CD(CDo, epsilon(e, AR), CL(CLa(AR), Alpha)) * (1 / 2 * rho * V ** 2) * S - m * g * math.sin(Gam)
    f2 = CL(CLa(AR), Alpha) * (1 / 2 * rho * V ** 2) * S - m * g * math.cos(Gam)
    return f1, f2,

def FlightSim(Alpha, S, AR, e, m, CDo, g, rho):
    guess = [1, 0.1,]

    V, Gam = fsolve(equations, guess, args= (Alpha, S, AR, e, m, CDo, g, rho))
    return (abs(V), Gam)

def GroundSpeed(Alpha, S, AR, e, m, CDo, g, rho):
    V, Gam = FlightSim(Alpha, S, AR, e, m, CDo, g, rho)
    xdot = V * math.cos(Gam) # Ground Speed
    ydot = V * math.sin(Gam) # Sink Rate
    return -1 * abs(xdot)   # Negative was added so I could use the minimize function to maximize

def main():
    S = 0.017
    AR = 5
    e = 0.9
    m = 0.003
    CDo = 0.02
    g = 9.81
    rho = 1.225

    Alpha = 0.01745
    V, Gam = FlightSim(Alpha, S, AR, e, m, CDo, g, rho)
    gspeed = GroundSpeed(Alpha, S, AR, e, m, CDo, g, rho)
    print("At", Alpha,"radians:")
    print("\tSpeed is:", V)
    print("\tGamma is:", Gam)
    print("\tGround Speed is:", gspeed)
    print()

    Alpha = 0.052
    V, Gam = FlightSim(Alpha, S, AR, e, m, CDo, g, rho)
    gspeed = GroundSpeed(Alpha, S, AR, e, m, CDo, g, rho)
    print("At", Alpha,"radians:")
    print("\tSpeed is:", V)
    print("\tGamma is:", Gam)
    print("\tGround Speed is:", gspeed)
    print()

    Alpha = 0.001745
    V, Gam = FlightSim(Alpha, S, AR, e, m, CDo, g, rho)
    gspeed = GroundSpeed(Alpha, S, AR, e, m, CDo, g, rho)
    print("At", Alpha,"radians:")
    print("\tSpeed is:", V)
    print("\tGamma is:", Gam)
    print("\tGround Speed is:", gspeed)
    print()

if __name__ == "__main__":
    main()