from scipy.optimize import minimize
import numpy as np
import matplotlib.pyplot as plt
from functions import GroundSpeed, FlightSim

def main():
    S = 0.017
    AR = 5
    e = 0.9
    m = 0.003
    CDo = 0.02
    g = 9.81
    rho = 1.225

    guess = -1,

    answer = minimize(GroundSpeed, guess, args= (S, AR, e, m, CDo, g, rho), method='Nelder-Mead')
    print(answer.x)

main()