"Luke Vaughan"

import numpy as np

# standard PyQt5 imports
import sys
from PyQt5.QtWidgets import QDialog, QApplication
from PyQt5.QtWidgets import QFileDialog, QMessageBox
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtGui import QCursor
from PyQt5.QtCore import Qt, QEvent

# standard OpenGL imports
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from OpenGL_2D_class import gl2D, gl2DText, gl2DCircle, gl2DArc

# the ui created by Designer and pyuic
from Simple_GL_py import Ui_Dialog


class main_window(QDialog):
    def __init__(self):
        super(main_window, self).__init__()
        self.ui = Ui_Dialog()
        # setup the GUI
        self.ui.setupUi(self)

        # define any data (including object variables) your program might need
        # none needed in this simple example

        # create and setup the GL window object
        self.setupGLWindows()

        # and define any Widget callbacks (buttons, etc) or other necessary setup
        self.assign_widgets()

        # show the GUI
        self.show()

    def assign_widgets(self):  # callbacks for Widgets on your GUI
        self.ui.Exit.clicked.connect(self.ExitApp) #exits the appliocation

    def ExitApp(self): #function that is called if the exit button is called
        app.exit()

    # Setup OpenGL Drawing and Viewing
    def setupGLWindows(self):  # setup all GL windows
        # send it the   GL Widget     and the drawing Callback function
        self.glwindow1 = gl2D(self.ui.openGLWidget, self.DrawingCallback)

        # set the drawing space:    xmin  xmax  ymin   ymax
        self.glwindow1.setViewSize(-10, 130, -10, 130, allowDistortion=False)


    def DrawingCallback(self):
        # this is what actually draws the picture

        glColor3f(0, 0, 0)
        glLineWidth(3)
        glBegin(GL_LINE_STRIP)
        glVertex2f(30, 0)
        glVertex2f(90, 0)
        glEnd()

        glBegin(GL_LINE_STRIP)
        glVertex2f(30, 120)
        glVertex2f(90, 120)
        glEnd()

        glBegin(GL_LINE_STRIP)
        glVertex2f(24, 114)
        glVertex2f(24, 6)
        glEnd()

        glBegin(GL_LINE_STRIP)
        glVertex2f(96, 114)
        glVertex2f(96, 6)
        glEnd()

        gl2DArc(30, 114, 6, 90, 180)
        gl2DArc(90, 114, 6, 0, 90)
        gl2DArc(30, 6, 6, 180, 270)
        gl2DArc(90, 6, 6, 270, 360)
        gl2DCircle(60, 35, 9)
        gl2DCircle(60, 35, 20)

        glBegin(GL_LINE_STRIP)
        glVertex2f(30, 69)
        glVertex2f(90, 69)
        glVertex2f(90, 113)
        glVertex2f(30, 113)
        glVertex2f(30, 69)
        glEnd()

        gl2DText('Luke Vaughan', 45, 91)

if __name__ == "__main__":
    app = QApplication.instance()
    if not app:
        app = QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater)
    main_win = main_window()
    sys.exit(app.exec_())
