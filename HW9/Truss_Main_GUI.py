import numpy as np

import sys
from PyQt5.QtWidgets import QDialog, QApplication
from PyQt5.QtWidgets import QFileDialog, QMessageBox
from PyQt5.QtGui import QCursor
from PyQt5.QtCore import Qt

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from OpenGL_2D_class import gl2D, gl2DText, gl2DCircle

from Truss_Design_ui import Ui_Dialog
from Truss_Class import Truss


class main_window(QDialog):
    def __init__(self):
        super(main_window, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.assign_widgets()

        self.setupGLWindows()  # run setupGLWindows method

        self.truss = Truss()

        # define two data items
        self.truss = None
        self.filename = None

        self.show()

    def assign_widgets(self):
        self.ui.pushButton_Exit.clicked.connect(self.ExitApp)
        self.ui.pushButton_GetWing.clicked.connect(self.GetTruss)

    def setupGLWindows(self):
        self.glwindow1 = gl2D(self.ui.openGLWidget, self.DrawingCallback)
        self.glwindow1.setViewSize(-10, 500, -10, 500, allowDistortion=False)

    def DrawingCallback(self):
        # This is what actually draws the picture
        if self.truss is not None:
            self.truss.DrawTrussPicture()

    def GetTruss(self):
        # get the filename using the OPEN dialog
        self.filename = QFileDialog.getOpenFileName()[0]
        if len(self.filename) == 0:
            no_file()
            return
        self.ui.textEdit_filename.setText(self.filename)
        app.processEvents()

        # Read the file
        f1 = open(self.filename, 'r')  # open the file for reading
        data = f1.readlines()  # read the entire file as a list of strings
        f1.close()  # close the file  ... very important

        self.truss = Truss()  # Create an instance for Truss

        t = self.truss  # a shorter name for convenience

        t.ReadTrussData(data)
        rpt = t.GenerateReport()

        # put the report in the large TextBox
        self.ui.textEdit_Report.setText(rpt)

        # fill the small text boxes
        self.ui.lineEdit_name.setText(t.longestLink.name)
        self.ui.lineEdit_node1name.setText(t.longestLink.node1.name)
        self.ui.lineEdit_node2name.setText(t.longestLink.node2.name)
        self.ui.lineEdit_length.setText('{:8.2f}'.format(t.longest))

        # Draw truss
        [xmin, xmax, ymin, ymax] = self.truss.drawingsize
        dx = xmax - xmin
        dy = ymax - ymin
        xmin -= .05*dx
        xmax += .05*dx
        ymin -= .05*dy
        ymax += .05*dy
        self.glwindow1.setViewSize(xmin, xmax, ymin, ymax, allowDistortion=False)

        self.glwindow1.glUpdate()


    def ExitApp(self):
        app.exit()


def no_file():
    msg = QMessageBox()
    msg.setText('There was no file selected')
    msg.setWindowTitle("No File")
    retval = msg.exec_()
    return None


def bad_file():
    msg = QMessageBox()
    msg.setText('Unable to process the selected file')
    msg.setWindowTitle("Bad File")
    retval = msg.exec_()
    return None


if __name__ == "__main__":
    app = QApplication.instance()
    if not app:
        app = QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater)
    main_win = main_window()
    sys.exit(app.exec_())

