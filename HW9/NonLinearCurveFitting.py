import matplotlib.pyplot as plt                         # Import used to plot
from scipy.optimize import minimize                     # Import used to minimize function
import numpy as np                                      # Import used to deal with arrays
import math                                             # Import used to deal with exp()

# Defines the function to be minimized
def myfunc(guess, t, data):
    A, B, C = guess                                     # Imports the guesses
    f = 0                                               # Initializes value of the function
    for i in range(len(t)):                             # Loops through all values of t
        f += (expfunc(t[i], A, B, C) - data[i])**2      # Defines the error squared
    return f


# Defines the standard exponential function
def expfunc(t, A, B, C):
    return A + B * math.exp(C*t)                        # Returns the value of the exponential function

# Defines the function that plots the data along with the curve fit
def plotanswer(t, data, answer):
    A, B, C = answer                                    # Imports coefficients
    x = np.linspace(t[0], t[len(t)-1], 1000)            # Initializes x values
    y = np.zeros_like(x)                                # Initializes y values
    for i in range(len(x)):                             # Loops through all x values
        y[i] = A + B * math.exp(C*x[i])                 # Stores the y value for each x
    plt.plot(x, y, 'r')                                 # Plots the x and y values on a red line
    plt.plot(t, data, 'bs')                             # Plots data points on blue squares
    plt.legend(['Curve Fit','Data Points'])             # Gives the plot a legend
    plt.show()                                          # Shows the plot window

# Defines the main function
def main():
    t = [0, 0.333, 0.667, 1, 1.333, 1.667]                  # Initializes time
    data1 = [0, 0.363, 0.659, 0.902, 1.101, 1.264]          # Initializes data1
    data2 = [-1, -1.416, -1.746, -2.007, -2.214, -2.377]    # Initializes data2

    guess = [1, 1, -1]                                                          # Declares initial guess
    answer = minimize(myfunc, guess, args=(t,data1), method='Nelder-Mead')      # Performs minimization method
    # print(answer.x)
    guess2 = answer.x                                                           # New guess becomes previous answers
    answer2 = minimize(myfunc, guess2, args=(t,data1), method='Nelder-Mead')    # Performs minimization method
    print("The coefficients of data1 are:", answer2.x)                          # Prints the values of the coefficients
    plotanswer(t, data1, answer2.x)                                             # Plots the answers against the data

    guess = [1, 1, -1]                                                          # Declares initial guess
    answer = minimize(myfunc, guess, args=(t,data2), method='Nelder-Mead')      # Performs minimization method
    # print(answer.x)
    guess2 = answer.x                                                           # New guess becomes previous answer
    answer2 = minimize(myfunc, guess2, args=(t,data2), method='Nelder-Mead')    # Performs minimization method
    print("The coefficients of data2 are:", answer2.x)                          # Prints the values of the coefficients
    plotanswer(t, data2, answer2.x)                                             # Plots the answers against the data

main()