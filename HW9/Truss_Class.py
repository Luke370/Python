import numpy as np

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from OpenGL_2D_class import gl2D, gl2DText, gl2DCircle


class Node:
    def __init__(self):
        self.name = None
        self.x = None
        self.y = None


class Link:
    def __init__(self):
        self.name = None
        self.node1name = None
        self.node2name = None
        self.node1 = None
        self.node2 = None
        self.length = None
        self.angle = None


class Truss:
    def __init__(self, ):
        self.title = None
        self.Sut = None
        self.Sy = None
        self.E = None
        self.FSstatic = None
        self.nodes = []  # an empty list of nodes
        self.links = []  # an empty list of links
        self.drawingsize = None
        self.longest = -99999999
        self.longestLink = None

    def ReadTrussData(self, data):
        # data is an array of strings, read from a Truss data file
        for line in data:  # loop over all the lines
            cells = line.strip().split(',')
            keyword = cells[0].lower()

            if keyword == 'title': self.title = cells[1].replace("'", "")
            if keyword == 'static_factor': self.FSstatic = float(cells[1])

            if keyword == 'material':
                self.Sut = float(cells[1])
                self.Sy = float(cells[2])
                self.E = float(cells[3])

            if keyword == 'node':
                thisnode = Node()
                thisnode.name = cells[1].strip()
                thisnode.x = float(cells[2].strip())
                thisnode.y = float(cells[3].strip())
                self.nodes.append(thisnode)

            if keyword == 'link':
                thislink = Link()
                thislink.name = cells[1].strip()
                thislink.node1name = cells[2].strip()
                thislink.node2name = cells[3].strip()
                self.links.append(thislink)

        # end for line
        self.UpdateConnections()

    def UpdateConnections(self):
        # get the node info
        for link in self.links:
            for node in self.nodes:
                if node.name == link.node1name:
                    link.node1 = node
                if node.name == link.node2name:
                    link.node2 = node
        # Next Link
        self.longest = -9999999
        self.longestLink = None
        for link in self.links:
            x1 = link.node1.x
            y1 = link.node1.y
            x2 = link.length = link.node2.x
            y2 = link.length = link.node2.y
            link.length = np.sqrt((x2-x1)**2 + (y2-y1)**2)
            link.angle = np.arctan2((y2-y1), (x2-x1))

            if link.length > self.longest:
                self.longest = link.length
                self.longestLink = link
        # Next Link

        # Loop over all Nodes to estimate the drawing size
        xmin, xmax, ymin, ymax = (1e12, -1e12, 1e12, -1e12)
        for node in self.nodes:
            if node.x <= xmin: xmin = node.x
            if node.x >= xmax: xmax = node.x
            if node.y <= ymin: ymin = node.y
            if node.y >= ymax: ymax = node.y
        self.drawingsize = [xmin, xmax, ymin, ymax]

    def GenerateReport(self):

        rpt = '                      Truss Design Report\n'
        rpt += '\nTitle: {}\n'.format(self.title)
        rpt += '\nStatic Factor of Safety: {:6.2f}'.format(self.FSstatic)
        rpt += '\nUltimate Strength: {:9.1f}'.format(self.Sut)
        rpt += '\nYield Strength: {:9.1f}'.format(self.Sy)
        rpt += '\nModulus of Elasticity: {:6.1f}'.format(self.E)
        rpt += '\n\n'

        rpt += '------------------- Link Summary -------------------------\n'
        rpt += '\nLink (1)    (2)      Length      Angle\n'
        for link in self.links:
            rpt += '{:6}{:7}{:7}'.format(link.name, link.node1name, link.node2name)
            rpt += '{:7.2f}  '.format(link.length)
            rpt += '{:10.2f}  \n'.format(link.angle)
        rpt += '\n\n'

        return rpt

    def DrawTrussPicture(self):
        # Draw the links
        for link in self.links:
            glColor3f(0, 1, 0)
            glLineWidth(1)
            glBegin(GL_LINE_STRIP)
            glVertex2f(link.node1.x, link.node1.y)
            glVertex2f(link.node2.x, link.node2.y)
            glEnd()

        # Draw and label nodes
        xmin, xmax, ymin, ymax = self.drawingsize
        radius = (xmax - xmin)/60
        for node in self.nodes:
            glColor3f(0, 3, 0)
            gl2DCircle(node.x, node.y, radius, fill=True)
            glColor3f(0, 0, 0)
            gl2DText(node.name, node.x, node.y)


        # Put names of contributors on the drawing
        xcenter = (xmax - xmin) / 2
        gl2DText('Dade Eddy', xmax, ymax)
        gl2DText('Brenden Dominick', xmax, ymax-4*radius)
        gl2DText('Luke Vaughan', xmax, ymax-8*radius)
