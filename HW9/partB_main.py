"Brenden Dominick"
"Dade Eddy"
"Luke Vaughan"

import numpy as np

# standard PyQt5 imports
import sys
from PyQt5.QtWidgets import QDialog, QApplication
from PyQt5.QtWidgets import QFileDialog, QMessageBox
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtGui import QCursor
from PyQt5.QtCore import Qt, QEvent

# standard OpenGL imports
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from OpenGL_2D_class import gl2D, gl2DText, gl2DCircle

# the ui created by Designer and pyuic
from Simple_GL_py import Ui_Dialog


class main_window(QDialog):
    def __init__(self):
        super(main_window, self).__init__()
        self.ui = Ui_Dialog()
        # setup the GUI
        self.ui.setupUi(self)

        # define any data (including object variables) your program might need
        # none needed in this simple example

        # create and setup the GL window object
        self.setupGLWindows()

        # and define any Widget callbacks (buttons, etc) or other necessary setup
        self.assign_widgets()

        # show the GUI
        self.show()

    def assign_widgets(self):  # callbacks for Widgets on your GUI
        self.ui.Exit.clicked.connect(self.ExitApp) #exits the appliocation

    def ExitApp(self): #function that is called if the exit button is called
        app.exit()

    # Setup OpenGL Drawing and Viewing
    def setupGLWindows(self):  # setup all GL windows
        # send it the   GL Widget     and the drawing Callback function
        self.glwindow1 = gl2D(self.ui.openGLWidget, self.DrawingCallback)

        # set the drawing space:    xmin  xmax  ymin   ymax
        self.glwindow1.setViewSize(-10, 130, -10, 112, allowDistortion=False)


    def DrawingCallback(self):
        # this is what actually draws the picture

        # Draws the main body
        glColor3f(0, 0, 0)
        glLineWidth(3)
        glBegin(GL_LINE_STRIP)  # begin drawing connected lines
        # use GL_LINE for drawing a series of disconnected lines
        glVertex2f(0, 0)        #Point 1
        glVertex2f(0, 44)       #Point 2
        glVertex2f(33, 77)      #Point 3
        glVertex2f(78, 77)      #Point 4
        glVertex2f(120, 44)     #Point 5
        glVertex2f(120, 0)      #Point 6
        glVertex2f(0, 0)        #Return to Point 1
        glEnd()

        # draw the Circle
        glColor3f(0, 0, 0) #color of the circle
        glLineWidth(2) #Line width for the circle outside
        gl2DCircle(36, 44, 18, fill=False) #location and dimensions of the circle, no infill


        # Draw the second circle
        glColor3f(0, 0, 0) #color white selected
        glLineWidth(2) #line width
        gl2DCircle(84, 20, 10, fill=False) #location and dimensions of the circle, no infill

        glColor3f(1, 1, 1)  #Color of the names
        gl2DText('Brenden Dominick', 0.0,105) #first name and location
        gl2DText('Dade Eddy', 0.0, 100) #second name and location
        gl2DText('Luke Vaughan', 0.0, 95) #third name and location

if __name__ == "__main__":
    app = QApplication.instance()
    if not app:
        app = QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater)
    main_win = main_window()
    sys.exit(app.exec_())
