# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Truss_Design_ui.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(1084, 682)
        self.pushButton_Exit = QtWidgets.QPushButton(Dialog)
        self.pushButton_Exit.setGeometry(QtCore.QRect(450, 640, 112, 34))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.pushButton_Exit.setFont(font)
        self.pushButton_Exit.setObjectName("pushButton_Exit")
        self.groupBox = QtWidgets.QGroupBox(Dialog)
        self.groupBox.setGeometry(QtCore.QRect(10, 10, 961, 101))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.groupBox.setFont(font)
        self.groupBox.setObjectName("groupBox")
        self.pushButton_GetWing = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_GetWing.setGeometry(QtCore.QRect(10, 30, 311, 34))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.pushButton_GetWing.setFont(font)
        self.pushButton_GetWing.setObjectName("pushButton_GetWing")
        self.textEdit_filename = QtWidgets.QTextEdit(self.groupBox)
        self.textEdit_filename.setGeometry(QtCore.QRect(380, 20, 571, 71))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.textEdit_filename.setFont(font)
        self.textEdit_filename.setReadOnly(True)
        self.textEdit_filename.setObjectName("textEdit_filename")
        self.groupBox_2 = QtWidgets.QGroupBox(Dialog)
        self.groupBox_2.setGeometry(QtCore.QRect(10, 110, 1011, 191))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.groupBox_2.setFont(font)
        self.groupBox_2.setObjectName("groupBox_2")
        self.groupBox_3 = QtWidgets.QGroupBox(self.groupBox_2)
        self.groupBox_3.setGeometry(QtCore.QRect(690, 20, 301, 141))
        self.groupBox_3.setObjectName("groupBox_3")
        self.lineEdit_node2name = QtWidgets.QLineEdit(self.groupBox_3)
        self.lineEdit_node2name.setGeometry(QtCore.QRect(170, 80, 113, 22))
        self.lineEdit_node2name.setObjectName("lineEdit_node2name")
        self.lineEdit_node1name = QtWidgets.QLineEdit(self.groupBox_3)
        self.lineEdit_node1name.setGeometry(QtCore.QRect(170, 50, 113, 22))
        self.lineEdit_node1name.setObjectName("lineEdit_node1name")
        self.lineEdit_name = QtWidgets.QLineEdit(self.groupBox_3)
        self.lineEdit_name.setGeometry(QtCore.QRect(170, 20, 113, 22))
        self.lineEdit_name.setObjectName("lineEdit_name")
        self.lineEdit_length = QtWidgets.QLineEdit(self.groupBox_3)
        self.lineEdit_length.setGeometry(QtCore.QRect(170, 110, 113, 22))
        self.lineEdit_length.setObjectName("lineEdit_length")
        self.label = QtWidgets.QLabel(self.groupBox_3)
        self.label.setGeometry(QtCore.QRect(10, 20, 141, 16))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.groupBox_3)
        self.label_2.setGeometry(QtCore.QRect(10, 50, 141, 16))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.groupBox_3)
        self.label_3.setGeometry(QtCore.QRect(10, 80, 141, 16))
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.groupBox_3)
        self.label_4.setGeometry(QtCore.QRect(10, 110, 141, 16))
        self.label_4.setObjectName("label_4")
        self.textEdit_Report = QtWidgets.QTextEdit(self.groupBox_2)
        self.textEdit_Report.setGeometry(QtCore.QRect(10, 20, 671, 161))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.textEdit_Report.setFont(font)
        self.textEdit_Report.setReadOnly(True)
        self.textEdit_Report.setObjectName("textEdit_Report")
        self.groupBox_4 = QtWidgets.QGroupBox(Dialog)
        self.groupBox_4.setGeometry(QtCore.QRect(10, 300, 1011, 331))
        self.groupBox_4.setObjectName("groupBox_4")
        self.openGLWidget = QtWidgets.QOpenGLWidget(self.groupBox_4)
        self.openGLWidget.setGeometry(QtCore.QRect(10, 20, 991, 301))
        self.openGLWidget.setObjectName("openGLWidget")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Truss Structural Design"))
        self.pushButton_Exit.setText(_translate("Dialog", "Exit"))
        self.groupBox.setTitle(_translate("Dialog", "Truss File and Load Set"))
        self.pushButton_GetWing.setText(_translate("Dialog", "Open and Read a Truss File"))
        self.textEdit_filename.setHtml(_translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">This is a text edit</p></body></html>"))
        self.groupBox_2.setTitle(_translate("Dialog", "Design Report"))
        self.groupBox_3.setTitle(_translate("Dialog", "Longest Link"))
        self.label.setText(_translate("Dialog", "Link Name"))
        self.label_2.setText(_translate("Dialog", "Node1 Name"))
        self.label_3.setText(_translate("Dialog", "Node2 Name"))
        self.label_4.setText(_translate("Dialog", "Link Length"))
        self.textEdit_Report.setHtml(_translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">This is a text edit</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.groupBox_4.setTitle(_translate("Dialog", "Truss Drawing"))

