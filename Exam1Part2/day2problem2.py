"""
Luke Vaughan
2-14-20
"""

import math


def STO(weight):
    thrust = 15000
    S = 900
    CLmax = 2.4
    CD = 0.0279
    rho = 0.002377
    gc = 32.2

    Vstall = math.sqrt(weight / (0.5 * rho * S * CLmax))
    VTO = 1.2 * Vstall
    A = gc * (thrust / weight)
    B = gc / weight * (0.5 * rho * S * CD)
    func = lambda V: V / (A - B * V ** 2)
    STO = Simpson(func, 0, VTO, 1000)
    return STO


def maxweight(maxtakeoff):
    func = lambda weight: STO(weight) - maxtakeoff
    return Secant(func, 1000, 50000, 1000, 0.001)


def Simpson(fnc, a, b, npoints):
    # Declarations
    Simp = 0
    # Checks if the number of points is odd, and if its isn't it adds an extra point
    if npoints % 2 == 0:
        divisions = npoints + 1
    else:
        divisions = npoints

    # Creates the spacing between the rectangles
    deltax = (b - a) / (divisions - 1)
    # Initializes x
    x = a

    # Loops through the evenly spaced x values and performs simpson's rule
    for i in range(divisions):
        # For the first end point and the last end point the function is multiplied by 1
        if i == 0 or i == divisions - 1:
            Simp += fnc(x)
            x += deltax
        # For the 2nd,4th,6th... x values the function will be multiplied by 4
        elif i % 2 == 1:
            Simp += 4 * fnc(x)
            x += deltax
        # For the 3rd,5th,7th... x values the function will be multiplied by 2
        else:
            Simp += 2 * fnc(x)
            x += deltax
    # Our result of simpson's rule is the sum of all the function values which has been stored in Simp and then needs
    # to be multiplied by deltax/3
    Simp = deltax / 3 * Simp
    # Returns the value of Simp and the number of subdivisions
    return Simp


def Secant(fcn, x0, x1, maxiter, xtol):
    iter = 0
    xn = x0
    xnminus1 = x1
    while abs(xn - xnminus1) > xtol and iter <= maxiter:
        xnplus1 = xn - fcn(xn) * (xn - xnminus1) / (fcn(xn) - fcn(xnminus1))
        xnminus1 = xn
        xn = xnplus1
        iter += 1
    return xn


def main():
    # weight must be an integer due to the way I called it in the .format
    weight = 55000
    print("At a weight of {:d} pounds, the take off distance is {:.2f} ft.".format(weight, STO(weight)))
    # maxtakeoff must be an integer due to the way I called it in the .format
    maxtakeoff = 1250
    print("For a max takeoff distance of {:d}, the max weight will be {:.2f} pounds.".format(maxtakeoff,
                                                                                             maxweight(maxtakeoff)))
    # maxtakeoff must be an integer due to the way I called it in the .format
    maxtakeoff = 2000
    print("For a max takeoff distance of {:d}, the max weight will be {:.2f} pounds.".format(maxtakeoff,
                                                                                             maxweight(maxtakeoff)))


main()
