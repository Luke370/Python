import matplotlib.pyplot as plt
import numpy as np

def main():
    temp, pressure, hf, hg, sf, sg, vf, vg = np.loadtxt('sat_water_table.txt', skiprows=1, unpack=True)
    plt.plot(pressure, temp)
    plt.ylabel("Temperature")
    plt.xlabel("Pressure")
    plt.title("Temperature vs Pressure")
    plt.show()

main()