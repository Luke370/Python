import numpy as np
import matplotlib.pyplot as plt

class LeastSquares:
    def __init__(self, xvals, yvals, name = None):
        self.xvals = xvals
        self.yvals = yvals
        self.name = name
        self.power = None
        self.coeffs = None

    def Power(self, power):
        self.power = power
        self.getCoeffs(self.power)
        return None

    def getCoeffs(self, power):
        # Finds the max power of x sums that we must find
        sx_max = power * 2
        # Declares and Initializes some numpy matrices and arrays
        sx = np.zeros(sx_max + 1)
        b = np.zeros((power + 1, 1))
        A = np.zeros((power + 1, power + 1))

        # Iterates through the values to find their sum to the i-th power
        for i in range(sx_max + 1):
            sx[i] = np.sum(np.power(self.xvals, i))

        # Iterates through the values to find the product of y with x to the power of i.
        for i in range(power + 1):
            b[i, 0] = np.sum(np.multiply(self.yvals, np.power(self.xvals, i)))

        # Forms the matrix of the sx values
        offset = 0
        for row in range(power + 1):
            for col in range(power + 1):
                A[row, col] = sx[col + offset]
            offset += 1

        # Augments A and b to send to GaussElim
        z = np.hstack((A, b))
        # Note z must be a list and not a numpy ndarray. Must use .tolist() function to convert to correct data type!
        # Returns the coefficients of Least Squares
        self.coeffs = np.linalg.solve(A, b)
        return self.coeffs

    def Print(self):
        print("Least Squares dataset:", self.name)
        print("x-values", self.xvals)
        print("y-values", self.yvals)
        print("power:", self.power)
        print("coefficients:", self.coeffs)
        return None

    def Plot(self, showpoints = True):
        # Calls the LeastSquares function to get the coefficients of the polynomial
        coeff = LeastSquares(self.xvals, self.yvals, self.power)

        # Initializes u and v
        npoints = 500
        u = np.linspace(min(self.xvals), max(self.xvals), npoints)
        v = np.zeros_like(u)

        # Determines the value of v given the coefficients and u value
        for i in range(len(u)):
            for j in range(len(self.coeffs)):
                v[i] += self.coeffs[j] * (u[i]) ** j

        # plots u and v in a green line
        plt.plot(u, v, 'g')

        # Determines if it should show the original data points and adds the correct legend
        if showpoints == True:
            plt.plot(self.xvals, self.yvals, 'bs')
            plt.legend(['Least Squares Curve Fit', 'Data Points'])
        else:
            plt.legend(['Least Squares Curve Fit'])

        # Labels the x and y axes
        plt.xlabel('x')
        plt.ylabel('y')

        # Adds a title to the plot
        plt.title('Least Squares Curve Fit with a polynomial of degree {:d}.'.format(self.power))

        # Shows the plot in a figure window
        plt.show()
        return None

def main():
    x = np.array([0.05, 0.11, 0.15, 0.31, 0.46, 0.52, 0.7, 0.74, 0.82, 0.98, 1.17])
    y = np.array([0.956, 1.09, 1.332, 0.717, 0.771, 0.539, 0.378, 0.370, 0.306, 0.242, 0.104])

    lsq1 = LeastSquares(x,y,'sample data')
    lsq1.Power(1)
    lsq1.Print()
    lsq1.Plot()

    lsq1.Power(5)
    lsq1.Print()
    lsq1.Plot()

if __name__ == "__main__":
    main()