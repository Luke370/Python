#Dade Edday
#Brenden Dominick
#Luke Vaughn


#startup functions
import numpy as np
import matplotlib.pyplot as plt

import sys
from PyQt5.QtWidgets import QDialog, QApplication
from PyQt5.QtWidgets import QFileDialog, QMessageBox
from PyQt5.QtGui import QCursor
from PyQt5.QtCore import Qt
from PyQt5 import QtCore, QtGui, QtWidgets, uic

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

# Import functions
from StratoGlider_ui import Ui_Dialog
from StratoGlider_Class import * #Balloon_Flight, FindItemByName


class main_window(QDialog):
    def __init__(self):
        super(main_window, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.assign_widgets()
        plotwin = self.ui.graphicsView
        self.m = PlotCanvas(plotwin)
        self.m2 = PlotCanvas(self.ui.graphicsView2)
        self.show()
        #variables
        self.filename = None
        self.balloon_flight = None
        self.glider = None

    def assign_widgets(self):
        self.ui.pushButton_Exit.clicked.connect(self.ExitApp)
        self.ui.pushButton_GetWing.clicked.connect(self.GetFlight)
        self.ui.pushButton_Launch.clicked.connect(self.Launch)

    def GetFlight(self):
        self.filename = QFileDialog.getOpenFileName()[0]

        # if there is no input, then it gives an error that there is no file, gives popup to user
        if len(self.filename) == 0:
            no_file()
            return
        # Updates the textbox on the screen to showcase the name of the file
        self.ui.filelocation.setText(self.filename)
        app.processEvents()


        # Read the file
        f1 = open(self.filename, 'r')  # open the file for reading
        data = f1.readlines()  # read the entire file as a list of strings
        f1.close()  # close the file

        if len(self.filename) == 0:
            no_file()
            return


        self.balloon_flight = Balloon_Flight()  # Create an instance for Balloon Flight

        self.balloon_flight.ReadBalloonData(data)
        #self.balloon_flight.FlightSim(self, )

        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        try:
            self.ui.comboBox_Glider.clear()
            self.ui.comboBox_Wind.clear()
            self.ui.comboBox_Balloon.clear()
            # fill the small text boxes with the values from the given text box
            # this the beginning, the values in these boxes are set with the data from the data file simulation

            self.ui.textEdit_LaunchDia.setText(self.balloon_flight.launchdia)#'{:8.3f}'.format(self.balloon_flight.launchdia)  # Launch Diameter
            self.ui.textEdit_Altitude.setText(self.balloon_flight.altitude)  # Altitude
            self.ui.textBrowser_PayloadMass.setText(self.balloon_flight.payloadmass)  # Payload Mass

            #Sets Combo Box Balloon
            for i in range(len(self.balloon_flight.balloonlib)):
                b = self.balloon_flight.balloonlib[i]
                string = b.name
                self.ui.comboBox_Balloon.addItem(string)

            #sets combo Box Wind
            for i in range(len(self.balloon_flight.windlib)):
                b = self.balloon_flight.windlib[i]
                string = b.name
                self.ui.comboBox_Wind.addItem(string)

            #Sets Combo Box Glider
            for i in range(len(self.balloon_flight.gliderlib)):
                b = self.balloon_flight.gliderlib[i]
                string = str(b.name)
                self.ui.comboBox_Glider.addItem(string)

            # Fills the lower boxes with the beginning stages
            b = self.balloon_flight
            string = str(b.balloon)
            self.ui.comboBox_Balloon.setCurrentText(string)

            string = str(b.windname)
            self.ui.comboBox_Wind.setCurrentText(string)

            string = str(b.glidername)
            self.ui.comboBox_Glider.setCurrentText(string)

            string = str(b.maxtime)
            self.ui.textBrowser_FlightTime.setText(string)  # Flight Time

            string = str(b.nsteps)
            self.ui.textBrowser_NumberOfSteps.setText(string)  # Number of Steps

            # Overrides the cursor so it is no longer in the hour glass
            QApplication.restoreOverrideCursor()

        except:  # if the file is not able to conduct all the above statements then it will call the bad_file() function
            QApplication.restoreOverrideCursor()
            bad_file()

    def Launch(self):
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        launchdia = float(self.ui.textEdit_LaunchDia.toPlainText())
        payloadmass = float(self.ui.textBrowser_PayloadMass.toPlainText())
        maxtime = float(self.ui.textBrowser_FlightTime.toPlainText())
        maxalt = float(self.ui.textEdit_Altitude.toPlainText())
        points = float(self.ui.textBrowser_NumberOfSteps.toPlainText())
        balloon = str(self.ui.comboBox_Balloon.currentText())
        wind = str(self.ui.comboBox_Wind.currentText())
        glider = str(self.ui.comboBox_Glider.currentText())


        self.balloon_flight.launchdia = launchdia
        self.balloon_flight.payloadmass = payloadmass
        self.balloon_flight.maxtime = maxtime
        self.balloon_flight.altitude = maxalt
        self.balloon_flight.nsteps = points
        self.balloon_flight.balloon = balloon
        self.balloon_flight.windname = wind
        self.balloon_flight.glidername = glider

        self.glider = FindItemByName(self.balloon_flight.glidername, self.balloon_flight.gliderlib)

        wind = FindItemByName(self.balloon_flight.windname, self.balloon_flight.windlib)
        self.balloon_flight.windaltitudes = wind.altitude
        self.balloon_flight.windspeeds = wind.speed

        balloon = FindItemByName(self.balloon_flight.balloon, self.balloon_flight.balloonlib)
        self.balloon_flight.burstdia = balloon.burstdia

        self.balloon_flight.FlightSim(maxtime, maxaltitude=maxalt, npoints=points)

        alt = float(self.balloon_flight.altitude)
        range = float(self.balloon_flight.range)
        payload = float(self.balloon_flight.payloadmass)
        maxtime = float(self.balloon_flight.maxtime)
        npoints = float(self.balloon_flight.nsteps)
        self.glider.ReturnHome(alt, range, wind, payload, maxtime, npoints)

        # self.glider
        #May need to be put in later, after everything is calculated
        report = self.balloon_flight.GenerateReport(self.glider.simtime, self.glider.flightPath) #calls the StratoGlider_class to create the report printout for the GUI
        self.ui.textEdit_FlightReport.setText(report) #puts the report into the text box in the GUI

        #Graphing the outcomes of the chart
        self.PlotC()
        QApplication.restoreOverrideCursor()

    def ExitApp(self): #closes the application window and ends the process
        app.exit()

    def PlotC(self):  # Graphing the outcomes of the chart
        t = self.balloon_flight.simtime
        x = self.balloon_flight.flightPath

        self.m.plotit(t, x)

        t2 = self.glider.simtime
        x2 = self.glider.flightPath

        self.m2.plotglider(t2, x2, t, x)
        pass

class PlotCanvas(FigureCanvas):
    def __init__(self, parent, width=None, height=None, dpi=100):
        if width == None: width = parent.width() / 100
        if height == None: height = parent.height() / 100
        fig = Figure(figsize=(width, height), dpi=dpi)
        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

    def plotit(self, t, x):

        self.figure.clf()

        position = self.figure.add_subplot(211)
        position.plot(t, x[:, 0])
        position.plot(t, x[:, 2])
        position.set_title('Flight Path (Balloon)')
        position.legend(['x position','y position'], loc='upper left')
        position.set_xlabel('Time, s')
        position.set_ylabel('Position, m')
        self.draw()
        velocity = self.figure.add_subplot(212)
        velocity.plot(t, x[:, 1])
        velocity.plot(t, x[:, 3])
        velocity.set_title('Velocity vs Time (Balloon)')
        velocity.legend(['x velocity', 'y velocity'], loc='upper left')
        velocity.set_xlabel('Time, s')
        velocity.set_ylabel('Velocity, m/s')
        self.draw()

    def plotglider(self, t, x, tb, xb):
        self.figure.clf()
        position = self.figure.add_subplot(111)
        position.plot(x[:, 0], x[:, 1])
        position.plot(xb[:, 0], xb[:, 2])
        position.set_title('Flight Path (Glider)')
        position.set_xlabel('Position, m')
        position.set_ylabel('Height, m')
        position.legend(['Glider Path', 'Balloon Path'], loc='upper left')
        self.draw()

def no_file():
    msg = QMessageBox()
    msg.setText('There was no file selected')
    msg.setWindowTitle("No File")
    retval = msg.exec_()
    return None

def bad_file(): #If the statements are not able to read the data correctly from the text file, it will output bad file
    msg = QMessageBox()
    msg.setText('Unable to process the selected file')
    msg.setWindowTitle("Bad File")
    retval = msg.exec_()
    return None

if __name__ == "__main__":
    app = QApplication.instance()
    if not app:
        app = QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater)
    main_win = main_window()
    sys.exit(app.exec_())

