import numpy as np
from scipy.optimize import minimize
from scipy.interpolate import griddata, interp1d
from scipy.integrate import odeint
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import math


class Glider:
    def __int__(self):
        self.name = None
        self.S = None
        self.AR = None
        self.e = None
        self.m = None
        self.CDo = None
        self.simtime = None
        self.flightPath = None
        self.atmosphere = None

    def GliderPerformance(self, alpha, gc, rho, payload, windspeed):
        g = self
        eps = 1 / (np.pi * float(g.e) * float(g.AR))
        CLa = np.pi * float(g.AR) / (1 + np.sqrt(1 + (float(g.AR) / 2) ** 2))
        CL = CLa * alpha
        CD = float(g.CDo) + eps * CL ** 2
        totalmass = float(g.m) + payload

        num = 4 * gc ** 2 * totalmass ** 2
        denom = (CD * float(g.S) * rho) ** 2 + (CL * float(g.S) * rho) ** 2
        V = (num / denom) ** 0.25
        Gam = -np.arcsin((0.5 * CD * float(g.S) * V ** 2 * rho) / (gc * totalmass))
        xdot = V * np.cos(Gam)
        ydot = V * np.sin(Gam)
        glideRatio = -(xdot - windspeed) / ydot
        groundSpeed = xdot - windspeed
        return xdot, ydot, glideRatio, groundSpeed

    def MaxGlideRatio(self, gc, rho, payload, windspeed, alphaGuess):
        def objective(alpha):
            glideRatio = self.GliderPerformance(alpha[0], gc, rho, payload, windspeed)[2]
            return -glideRatio

        answer = minimize(objective, [alphaGuess], method = 'Nelder-Mead')
        return float(answer.x), -answer.fun

    def ReturnHome(self, altitude, distance, winds, payload, maxtime, npoints):
        def ode_system(X, t):
            nonlocal alphaGuess
            x = X[0]
            y = X[1]

            atm = Atmosphere()
            self.atmosphere = atm.GetData()

            if y < 0:
                y = 0

            windspeed = float(griddata(winds.altitude, winds.speed, y))
            gc = float(griddata(atm.hdata, atm.gdata, y))
            rho = float(griddata(atm.hdata, atm.rhodata, y))

            alpha = self.MaxGlideRatio(gc, rho, payload, windspeed, alphaGuess)[0]
            alphaGuess = alpha
            xdot, ydot, glideRatio, groundSpeed = self.GliderPerformance(alpha, gc, rho, payload, windspeed)

            return [-groundSpeed, ydot]

        alphaGuess = 0.1
        t = np.linspace(0, maxtime, npoints)
        ic = [distance, altitude]
        xy = RK4(ode_system, ic, t)

        for i in range(len(xy)):
            if xy[i, 1] < 0:
                t = t[0:i+1]
                xy = xy[0:i+1, :]
                break

        for i in range(len(xy)):
            if xy[i, 0] < 0:
                t = t[0:i + 1]
                xy = xy[0:i + 1, :]
                break

        self.simtime = t
        self.flightPath = xy

class WindLib:
    def __int__(self):
        self.name = None
        self.altitude = [None]
        self.speed = [None]


class BalloonLib:
    def __init__(self):
        self.name = None
        self.mass = None
        self.uninflateddia = None
        self.burstdia = None


class Atmosphere:
    def __init__(self):
        self.hdata = None
        self.tCdata = None
        self.gdata = None
        self.pdata = None
        self.rhodata = None
        self.mudata = None

    def GetData(self):
        alt, temp, g, pressure, rho, mu = np.loadtxt('US Standard Air Properties.txt', skiprows=4, unpack=True)
        self.hdata = alt
        self.tCdata = temp
        self.gdata = g
        self.pdata = pressure * 10 ** (4)
        self.rhodata = rho
        self.mudata = mu * 10 ** (-5)

class Balloon_Flight:
    def __init__(self):
        self.title = None
        self.maxtime = None
        self.nsteps = None
        self.gliderlib = []
        self.windlib = []
        self.balloonlib = []
        self.balloon = None
        self.launchdia = None
        self.glidername = None
        self.windname = None
        self.windspeeds = []
        self.windaltitudes = []
        self.payloadmass = None
        self.altitude = None
        self.simtime = []
        self.flightPath = []
        self.V_expectation = 0
        self.gasmass = None
        self.range = None
        self.diameter = None
        self.burstdia = None

    def FlightSim(self, maxtime, maxaltitude=None, npoints=600):
        self.simtime = np.linspace(0, maxtime, npoints)
        balloon = FindItemByName(self.balloon, self.balloonlib)
        balloonmass = balloon.mass

        balloon = FindItemByName(self.balloon, self.balloonlib)
        self.burstdia = balloon.burstdia

        def ode_system(X, t=self.simtime):
            x = X[0]
            xdot = X[1]
            y = X[2]
            ydot = X[3]

            index = self.FindAltitudeIndex(y)
            S2 = self.windspeeds[index + 1]
            S1 = self.windspeeds[index]
            h2 = self.windaltitudes[index + 1]
            h1 = self.windaltitudes[index]
            windvel = (S2 - S1) / (h2 - h1) * (y - h1) + S1
            if xdot > windvel:
                xdot = windvel
            if ydot < 0:
                ydot = 0
            if y > maxaltitude:
                y = maxaltitude
                x = self.range

            vRelx = xdot - windvel
            vRely = ydot
            alt = float(y)
            xforce, yforce = self.Forces(vRelx, vRely, alt)
            ydot = self.V_expectation
            mass = (float(balloonmass) + float(self.payloadmass) + float(self.gasmass))
            xddot = xforce / mass
            yddot = yforce / mass

            return [xdot, xddot, ydot, yddot]

        ic = (0, 0, 0, 0)

        def RK4(func, ic, t):
            ntimes = len(t)
            nstates = len(ic)
            x = np.zeros((ntimes, nstates))
            x[0] = ic
            for i in range(len(t) - 1):
                index = self.FindAltitudeIndex(x[i][2])
                S2 = self.windspeeds[index+1]
                S1 = self.windspeeds[index]
                h2 = self.windaltitudes[index+1]
                h1 = self.windaltitudes[index]
                windvel = (S2-S1)/(h2-h1)*(x[i][2]-h1)+S1
                V = self.V_expectation
                x[i][3] = V
                if abs(x[i][1]) > windvel:
                    x[i][1] = windvel
                if x[i][3] < 0:
                    x[i][3] = 0
                if x[i][2] > maxaltitude:
                    x[i][2] = maxaltitude
                    if self.range == None:
                        self.range = x[i][0]

                h = t[i + 1] - t[i]
                k1 = h * np.array(func(x[i]), t[i])
                k2 = h * np.array(func(x[i] + k1 / 2, t[i] + h / 2))
                k3 = h * np.array(func(x[i] + k2 / 2, t[i] + h / 2))
                k4 = h * np.array(func(x[i] + k3, t[i] + h))
                x[i + 1] = x[i] + (1 / 6) * (k1 + 2 * k2 + 2 * k3 + k4)
                pass
            return x

        position = RK4(ode_system, ic, self.simtime)
        position[-1,3] = position[-2, 3]
        self.flightPath = position

        return position


    def Forces(self, vRelx, vRely, altitude):
        alt, temp, g, pressure, rho, mu = np.loadtxt('US Standard Air Properties.txt', skiprows=4, unpack=True)
        h = altitude
        T = float(griddata(alt, temp, h))
        grav = float(griddata(alt, g, h))
        p = float(griddata(alt, pressure, h)) * 10 ** (4)
        density = float(griddata(alt, rho, h))
        visc = float(griddata(alt, mu, h)) * 10 ** (-5)

        R = 2077.1 # Assuming gas is helium units of [J/kg-K]
        initialvolume = (4*math.pi/3) * (float(self.launchdia)/2)**3
        self.gasmass = gasMass(pressure[1] * 10 **4, initialvolume, R, temp[1] + 273.15) # Assuming launch happens at 0 m.
        Vballoon = gasVolume(p, self.gasmass, R, T + 273)
        L = density * grav * Vballoon

        balloon = FindItemByName(self.balloon, self.balloonlib)
        balloonmass = float(balloon.mass)
        payloadmass = float(self.payloadmass)
        W = (float(balloonmass) + payloadmass) * grav
        dballoon = 2 * ((3*Vballoon)/(4*math.pi))**(1/3)
        self.diameter = dballoon
        self.V_expectation = ((L - W) * 2 / ( density * (math.pi/4) * dballoon ** 2 )) ** (1/2)
        V = self.V_expectation

        Rex = density * abs(vRelx) * dballoon / visc
        cd = CDSphere(Rex)
        Dx = 1/2 * cd * density * vRelx ** 2 * math.pi / 4 * dballoon ** 2

        Rey = density * abs(vRely) * dballoon / visc
        cd = CDSphere(Rey)
        Dy = 1 / 2 * cd * density * vRely ** 2 * math.pi / 4 * dballoon ** 2

        xforce = Dx
        yforce = L - W - Dy
        if L < abs(Dy):
            yforce = -W

        return xforce, yforce

    def FindAltitudeIndex(self, altitude):
        h = self.windaltitudes
        for i in range(len(h)):
            if altitude <= h[i]:
                if i == 0:
                    return 0
                else:
                    return i - 1

    def ReadBalloonData(self, data):
        #  Data is an array of strings, read from the duct design data file
        for line in data:  # Loop over all the lines
            cells = line.strip().split(',')  # Splits the cells by commas and strips whitespace
            keyword = cells[0].lower()  # Saves the first item in cells in lowercase as keyword

            if keyword.lower() == 'title':  # Stores the name of the title
                self.title = cells[1].replace("'", "")

            if keyword.lower() == 'simulation':  # Stores the value of Simulation
                thissim = self
                thissim.balloon = cells[1].strip()
                thissim.launchdia = cells[2].strip()
                thissim.glidername = cells[3].strip()
                thissim.windname = cells[4].strip()
                thissim.payloadmass = cells[5].strip()
                thissim.altitude = cells[6].strip()
                # may have to do some looping to include each set of data here.

            if keyword.lower() == 'simcontrol':
                self.maxtime = float(cells[1])
                self.nsteps = int(cells[2])

            if keyword.lower() == 'glider':  # Stores the value of Simulation
                thisglider = Glider()
                thisglider.name = cells[1].strip()
                thisglider.S = cells[2].strip()
                thisglider.AR = cells[3].strip()
                thisglider.e = cells[4].strip()
                thisglider.m = cells[5].strip()
                thisglider.CDo = cells[6].strip()
                self.gliderlib.append(thisglider)

            if keyword.lower() == 'windlib':  # list of values, alternating
                thiswind = WindLib()
                thiswind.name = cells[1].strip()
                h = np.zeros(int((len(cells) -2) / 2))
                V = np.zeros(int((len(cells) -2) / 2))
                for i in range(2, len(cells)):
                    if i % 2 == 0:
                        h[int(i/2 - 1)] = cells[i].replace("(", "")
                    if i % 2 == 1:
                        V[int((i-1) / 2) - 1] = cells[i].replace(")", "")
                thiswind.altitude = h
                thiswind.speed = V
                self.windlib.append(thiswind)

            if keyword.lower() == 'balloonlib':
                thisballoon = BalloonLib()
                thisballoon.name = cells[1].strip()
                thisballoon.mass = cells[2].strip()
                thisballoon.uninflateddia = cells[3].strip()
                thisballoon.burstdia = cells[4].strip()
                self.balloonlib.append(thisballoon)

        wind = FindItemByName(self.windname, self.windlib)
        self.windaltitudes = wind.altitude
        self.windspeeds = wind.speed
        self.UpdateConnections()

    def UpdateConnections(self):
        # self.balloon.setValue(comboBox_Balloon)
        pass

    def GenerateReport(self, t , x):
        report = '\t                   StratoGlider Flight Performance'  # Report Heading
        report += '\n\tTitle: ' + self.title  # Report title
        report += '\n\tBalloon Name: ' + str(self.balloon)  # Prints Balloon name
        # Takes the name from comboBox_Balloon
        report += '\n\tLaunch Diameter:' + str(self.launchdia) + ' meters'  # Prints Launch Diameter
        # takes from textBrowser_launchDia
        report += '\n\tPayload Mass: ' + str(self.payloadmass) + ' kg'  # Prints PayloadMass
        # Takes from textBrowser_PayloadMass
        report += '\n\tWind Condition: ' + str(self.windname)  # Prints Wind Condition
        # Takes from the comboBox_Wind
        report += '\n\tGlider Deployment Altitude: ' + str(self.altitude) + ' meters'
        # Takes from textBrowser_Altitude
        report += '\n\tGlider Name: ' + str(self.glidername) + ' nudes'
        report += '\n'
        report += '\n'
        report += '\n\t                   Balloon at Deployment Altitude'  # Title for Ballon at Deploymenty Altitude
        report += '\n\tFlight Time: ' + str(self.maxtime) + ' seconds'  # needs the value calculated for flight time of the balloon
        report += '\n\tGround Distance Traveled: ' + str(round(self.range, 2)) + ' meters'  # needs the distance traveled of the balloon(final distance)
        report += '\n\tFinal Diameter: ' + str(round(self.diameter, 2)) + ' meters'
        report += '\n\tBurst Diameter:' + str(self.burstdia) + ' meters'  # Final diameter of baloon and burst diameter

        # Glider Return Flight Section
        report += '\n'
        report += '\n'
        report += '\n\t\tGlider Return Flight'  # Title for the portion called Glider Return Flight
        report += '\n\tIt flys for ' + str(round(t[-1], 0)) + ' seconds'  # call for how long the glider flew for
        report += '\n\tIt reaches the ground ' + str(round(x[-1][0], 0)) + ' meters away from the launch site'  # Distance from the launch site

        return report

def FindItemByName(name, objectlist):
    for object in objectlist:
        if object.name == name:
            return object
    return None

def gasVolume(pressure, mass, R, T):
    return (mass * R * T) / pressure

def gasMass(pressure, volume, R, TC):
    return (pressure * volume) / (R * TC)

def CDSphere(RE):
    # Line of best fit according to https://pages.mtu.edu/~fmorriso/DataCorrelationForSphereDrag2016.pdf
    if 10**6 >= RE > 0.1:
        part1 = 24/RE
        part2 = 2.6*(RE/5.0)/(1+(RE/5.0)**1.52)
        part3 = 0.411*(RE/(2.63105))**(-7.94)/(1+(RE/(2.63*10**5))**(-8.00))
        part4 = 0.25*(RE/10**6)/(1+(RE/10**6))
        return part1 + part2 + part3 + part4
    # "For Reynolds values greater than 106, the relationship renders a drag coefficient of 0.2."
    if RE >= 10**6:
        return 0.2
    if 0 <= RE <=0.1:
        return 0
    else:
        return 0

def RK4(func, ic, t):
    ntimes = len(t)
    nstates = len(ic)
    x = np.zeros((ntimes, nstates))
    x[0] = ic
    for i in range(len(t) - 1):
        h = t[i + 1] - t[i]
        k1 = h * np.array(func(x[i], t[i]))
        k2 = h * np.array(func(x[i] + k1 / 2, t[i] + h / 2))
        k3 = h * np.array(func(x[i] + k2 / 2, t[i] + h / 2))
        k4 = h * np.array(func(x[i] + k3, t[i] + h))
        x[i + 1] = x[i] + (1 / 6) * (k1 + 2 * k2 + 2 * k3 + k4)
        pass
    return x

def main():
    import matplotlib.pyplot as plt
    f1 = open('Balloon 3.txt', 'r')  # open the file for reading
    data = f1.readlines()  # read the entire file as a list of strings
    f1.close()

    b = Balloon_Flight()
    b.ReadBalloonData(data)
    b.FlightSim(b.maxtime, maxaltitude=int(b.altitude), npoints=b.nsteps)

    t = b.simtime
    x = b.flightPath

    plt.plot(t, x[:, 0], 'b-', label='x')
    plt.plot(t, x[:, 2], 'r-', label='y')
    plt.legend(loc='upper left')
    plt.xlabel('Time (s)')
    plt.ylabel('x and y')
    plt.title('balloon x and y')
    plt.show()

    plt.plot(t, x[:, 1], 'b-', label='xdot')
    plt.plot(t, x[:, 3], 'r-', label='ydot')
    plt.legend(loc='upper left')
    plt.xlabel('Time (s)')
    plt.ylabel('x and y')
    plt.title('balloon x and y velocity ')
    plt.show()

if __name__ == '__main__':
    main()