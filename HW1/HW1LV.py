"""
Luke Vaughan
1/27/20
MAE 3403
HW#1
"""

"""
Problem #1:
Write a function defined as: def SequenceSumClosestTo(vals,thisSum):
vals: an array (list) containing numbers (integers and or floats)
thisSum: a number (integer or float)
return value: The starting location, length and sum of the first sequence of numbers in vals, whose sum is closest to thisSum.
"""
def SequenceSumClosestTo(vals,thisSum):
    # Declarations
    mysum = 0

    # Loops through each index
    for i in range(len(vals)):
        thissum = 0
        length = 0

        # Finds the sequence sum
        for j in range(len(vals) - i):
            # Checks to make sure thissum is less than the target sum
            if thissum + vals[j+i] <= thisSum:
                # Stores the length and the value of the current sum
                length += 1
                thissum += vals[j+i]
                # Checks to see if thissum is closer to the target than the previous iteration
                if thissum > mysum:
                    # Stores values of the closest sum so far
                    mysum = thissum
                    location = i
                    mylength = length
            # Goes to next i once thisSum is surpassed
            else: break

    # Returns values
    return location, mylength, mysum

"""
Problem #2
Write a function defined as: def MatrixSumLarge(vals, large):
vals: a matrix (list of lists) containing integers or floats
large: a positive float or integer. Any |value| >= this value is considered to be large.
return value: the sum of the values (not absolute values) of all the large entries in the matrix.
"""
def MatrixSumLarge(vals, large):
    # Declarations
    array = []
    k = 0
    # Iterates through each value in the matrix and stores any "large" values in list called array
    for i in range(len(vals)):
        for j in range(len(vals[i])):
            if abs(vals[i][j]) >= large:
                array.append(vals[i][j])
                k = k + 1
    # Returns the sum of the list array
    return sum(array)

"""
Problem #3 
Write a function defined as: def Horner(x, coeffs): 
x: as float 
coeffs: an array containing the coefficients of a polynomial, in order from the coefficient of the 
lowest power to the coefficient of the highest power. 
return value: the value of the polynomial for the given value of x, computed using Horner's Rule. 
"""
def Horner(x, coeffs):
    # Initialize Horner sequence!!!!!
    value = coeffs[len(coeffs)-1] * x + coeffs[len(coeffs)-2]
    # Perform HORNER SEQUENCE!!!!!! (for remaining coefficients)
    for i in range(len(coeffs)-3,-1,-1):
        value = (value*x) + int(coeffs[i])
    # RETURNS VALUE PRODUCED BY HORNER SEQUENCE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    return value    # :D

"""
Problem #4
Write a function defined as: def interp1D(x,xyvals,yvals):
x: a float or integer
xyvals, yvals: two arrays (lists) of floats or integers, representing columns of x and y values to be interpolated on.
return value: the linearly interpolated value from yvals, based on the location of the x value within xvals.
"""
def interp1D(x,xyvals,yvals):
    # Declarations
    placeholder = 0
    y = 0
    # Loops through the x values to find where our x value lies.
    for i in range(len(xyvals)):
        if xyvals[i] > x:
            placeholder = i - 1
    # Performs interpolation on the values to the right and to the left of the our x value.
    y = (int(yvals[placeholder])-int(yvals[placeholder-1]))/(int(xyvals[placeholder])-int(xyvals[placeholder-1]))*(x-int(xyvals[placeholder-1]))+int(yvals[placeholder-1])
    # Returns interpolated y.
    return y

def main():
    # define the variables needed to test the required function
    myvals = [1, 5, -2, 3, 5, 5, 3, 5, 2, -5, 3, 3, 1, 5]
    mymatrix = [[1, 3.7, -7, 4],
                [-8, -8, 2, -1.8],
                [-12, 7.9, 3.2, 12],
                ]
    a = [3, 2, -2, -4]
    xvals = [1, 2, 4, 5, 7]
    yvals = [2, 4, -2, 4, 5]
    x1 = 1.7
    x2 = 4.8
    mylarge = 5.2

    # for part a)
    start, length, sum = SequenceSumClosestTo(myvals,31)
    print('a) The {:d} term sequence starting at location {:d} sums to {:f}'.format(length, start, sum))

    # for part b)
    mysum = MatrixSumLarge(mymatrix, mylarge)
    print('b) Sum of small values = {:.1f}'.format(mysum))

    # for part c)
    poly = Horner(x1, a)
    print('c) Polynomial value for (x1= {:.2f}) = {:.1f}'.format(x1, poly))

    # for part d)
    y = interp1D(x2, xvals, yvals)
    print('d) Interpolated value for (x2= {:.2f}) = {:.3f}'.format(x2, y))

main()