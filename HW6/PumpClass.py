from scipy.optimize import fsolve

class Pump:
    def __init__(self, cvals):
        self.C = cvals  # coefficients of a cubic equation
        self.dp = None
        self.Q = None

    # The deltaP method computes and returns the pump exit pressure for the given flow
    # and saves the pressure and flow in self.dp and self.Q
    def deltaP(self, Q):
        self.Q = Q
        c = self.C
        self.dp = (((float(c[3]) * Q) + float(c[2])) * Q + float(c[1])) * Q + float(c[0])
        return self.dp

    # The flow method computes and returns the pump flow for the given pump pressure
    # and saves the pressure drop and flow in slf.dp and self.Q
    def flow(self, dp):
        def inverse(Q):
            func1 = self.deltaP(Q) - dp
            return func1
        self.Q = fsolve(inverse, 1)
        return float(self.Q)


def main():
    import numpy as np
    import matplotlib.pyplot as plt

    p1 = Pump([15000, -1820, -12900, -15150])
    dp = p1.deltaP(0.5)
    Q = p1.flow(dp)
    print(dp, Q)

    flows = np.linspace(0, 0.75, 21)
    dPs = p1.deltaP(flows)
    plt.plot(flows, dPs)
    plt.title('Pump Curve - Pressure (psf) vs Flow (cfs)')
    plt.xlabel('Flow')
    plt.ylabel('Pressure at pump exit')
    plt.show()

if __name__ == "__main__":
    main()
