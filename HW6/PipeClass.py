from scipy.optimize import fsolve
import numpy as np

class Pipe:
    def __init__(self, diameter, length, roughness, name = None):
        self.diameter = diameter
        self.length = length
        self.roughness = roughness
        self.name = name
        self.dp = None
        self.Q = None

    # The deltaP method computes and returns the pipe pressure drop for the given flow
    # and saves the pressure drop and flow in self.dp and self.Q
    def deltaP(self, flow, rho, mu):
        v = 4 * flow / (np.pi * self.diameter ** 2)
        Re = rho * v * self.diameter / mu
        self.dp = friction(self.roughness, self.diameter, Re) * self.length * abs(v) * v * rho/ (2 * self.diameter)
        return float(self.dp)

    # The flow method computes and returns the pipe flow for the given pressure drop
    # and saves the pressure drop and flow in self.dp and self.Q
    def flow(self, dp, rho, mu):
        def inverse(Q):
            func1 = self.deltaP(Q, rho, mu) - dp
            return func1
        self.Q = fsolve(inverse, 1)
        return float(self.Q)

    def print(self):
        print("The pressure drop in pipe", self.name, "is:", self.dp,
              "\n\tThe flow rate in pipe", self.name, "is:", self.Q)

def friction(eps, D, Re):
    term1 = 1/2.8257 * (eps/D)**1.1098 + 5.8506/(Re**0.8981)
    term2 = 5.0452/Re * np.log10(term1)
    term3 = -2.0*np.log10(eps/3.7065/D - term2)
    return term3**-2

def main():
    rho = 1.94; mu = 0.0000186; eps = 0.00082
    p1 = Pipe(6/12, 2000, eps, name = 'a-b')
    pdrop = p1.deltaP(0.75, rho, mu)
    Q = p1.flow(pdrop, rho, mu)
    print(pdrop, Q)
    p1.print()

if __name__ == "__main__":
    main()