from scipy.optimize import fsolve
import numpy as np

from PipeClass import Pipe
from PumpClass import Pump

# Solve the pipe network when the supply is a pressure source
def p1errors(pvals, pipelist, rho, mu, supplyp): # using a pressure source
    pb, pc, pd = pvals

    pab, pbc, pcd, pbd, pde = pipelist

    func1 = pab.flow(supplyp - pb, rho, mu) - pbd.flow(pb - pd, rho, mu) - pbc.flow(pb - pc, rho, mu)
    func2 = pbc.flow(pb - pc, rho, mu) - pcd.flow(pc - pd, rho, mu)
    func3 = pbd.flow(pb - pd, rho, mu) + pcd.flow(pc - pd, rho, mu) - pde.flow(pd, rho, mu)

    return func1, func2, func3

# Solve the pipe network when the supply is a flow source
def p2errors(pvals, pipelist, rho, mu, supplyQ): # using a flow source
    pa, pb, pc, pd = pvals

    pab, pbc, pcd, pbd, pde = pipelist

    func1 = pab.flow(pa-pb, rho, mu) - pbd.flow(pb-pd, rho, mu) - pbc.flow(pb-pc, rho, mu)
    func2 = pbc.flow(pb-pc, rho, mu) - pcd.flow(pc-pd, rho, mu)
    func3 = pbd.flow(pb-pd, rho, mu) + pcd.flow(pc-pd, rho, mu) - pde.flow(pd, rho, mu)
    func4 = pab.flow(pa-pb, rho, mu) - supplyQ

    return func1, func2, func3, func4

# Solve the pipe network when the supply is a pump
def p3errors(pvals, pipelist, rho, mu, pump):  # using a pump
    (pa, pb, pc, pd) = pvals

    pab, pbc, pcd, pbd, pde = pipelist

    func1 = pab.flow(pa - pb, rho, mu) - pbd.flow(pb - pd, rho, mu) - pbc.flow(pb - pc, rho, mu)
    func2 = pbc.flow(pb - pc, rho, mu) - pcd.flow(pc - pd, rho, mu)
    func3 = pbd.flow(pb - pd, rho, mu) + pcd.flow(pc - pd, rho, mu) - pde.flow(pd, rho, mu)
    func4 = pab.flow(pa - pb, rho, mu) - pump.flow(pa)

    return func1, func2, func3, func4

def main():
    rho = 1.94
    mu = 0.0000186
    eps = 0.00082

    ab = Pipe(4/12, 2500, eps, 'ab')
    bc = Pipe(5/12, 3500, eps, 'bc')
    cd = Pipe(6/12, 6000, eps, 'cd')
    bd = Pipe(8/12, 2000, eps, 'bd')
    de = Pipe(8/12, 1500, eps, 'de')

    pipelist = [ab,bc,cd,bd,de]

    # Pressure source
    """NOTE TO GRADER: I had to change the initial conditions for my code to work. However, now it works with only
    a runtime warning"""
    guess = (500, 400, 300)
    supplyp = 7596.4 #psf or 100 psi
    Pvals = fsolve(p1errors,guess,args=(pipelist,rho,mu,supplyp))
    # let's improve accuracy a little bit
    Pvals = fsolve(p1errors,Pvals,args=(pipelist,rho,mu,supplyp))
    pb, pc, pd = Pvals
    print('\nPressures at b, c and d are {:.1f}, {:.1f} and {:.1f}'.format(pb, pc, pd))
    for pipe in pipelist: pipe.print()
    print('Fsolve Errors: ', p1errors(Pvals, pipelist, rho, mu, supplyp))

    #flow source
    guess = [10000, 1000, 1100, 1200]
    supplyQ = 0.54932 #cfm
    Pvals = fsolve(p2errors, guess, args=(pipelist,rho,mu,supplyQ))
    pa, pb, pc, pd = Pvals
    print('\nPressures at a, b, c and d are {:.1f}, {:.1f}, {:.1f} and {:.1f}'.format(pa, pb, pc, pd))
    for pipe in pipelist: pipe.print()
    print('Fsolve Errors: ', p2errors(Pvals, pipelist, rho, mu, supplyQ))

    #pump source
    pump = Pump([15000, -1820, -12900, -15150]) #create the pump object

    guess = [130,120,110,100]
    Pvals = fsolve(p3errors, guess, args=(pipelist, rho, mu, pump))
    pa, pb, pc, pd = Pvals
    print('\nPressures at a, b, c and d are {:.1f}, {:.1f}, {:.1f} and {:.1f}'.format(pa, pb, pc, pd))
    for pipe in pipelist: pipe.print()
    print('Pump flow and pressure are: {:.3f} and {:.3f}'.format(float(pump.Q), float(pump.dp)))
    print('Fsolve Errors: ', p3errors(Pvals, pipelist, rho, mu, pump))

main()
