"""
Luke Vaughan
2-3-20
MAE 3403
HW2
"""
"""
Write a function as : def Simpson(fnc, a, b, npoints = 21):

Purpose: use Simpson's 1/3 rule to estimate the integral of fcn(x), between the limits of a and b.

fcn: the function we want to integrate
a and b: the lower and upper limits of integration
npoints: The number of integration points used in the range a to b (inclusive). Npoints must be an ODD number. If 
npoints is not ODD, then add 1 to make it odd!
return value: the estimate of the integral

Write and call a main() function that uses your Simpson function to estimate and print the integral of:
    x-3*cos(x)      with a = 1, b = 3 and npoints = 10
    cos(2x)*x^3     with a = 2, b = 3 and npoints = 23
"""
# Imported math so I can use the integrated cos function
import math

def Simpson(fnc, a, b, npoints):
    # Declarations
    Simp = 0
    divisions = 0
    value = 0

    # Checks if the number of points is odd, and if its isn't it adds an extra point
    if npoints%2 == 0:
        divisions = npoints + 1
    else:
        divisions = npoints

    # Creates the spacing between the rectangles
    deltax = (b-a)/(divisions-1)
    # Initializes x
    x = a

    # Loops through the evenly spaced x values and performs simpson's rule
    for i in range(divisions):
        # For the first end point and the last end point the function is multiplied by 1
        if i == 0 or i == divisions-1:
            Simp += fnc(x)
            x += deltax
        # For the 2nd,4th,6th... x values the function will be multiplied by 4
        elif i%2 == 1:
            Simp += 4*fnc(x)
            x += deltax
        # For the 3rd,5th,7th... x values the function will be multiplied by 2
        else:
            Simp += 2*fnc(x)
            x += deltax
    # Our result of simpson's rule is the sum of all the function values which has been stored in Simp and then needs
    # to be multiplied by deltax/3
    Simp = deltax/3 * Simp
    # Returns the value of Simp and the number of subdivisions
    return Simp, divisions-1

def main():
    # Defines the function for part A
    fnc = lambda x: x - 3 * math.cos(x)
    # Runs the function and prints the value
    Simp, divisions = Simpson(fnc, 1, 3, 10)
    print("The integral of x - 3 * cos(x) from 1 to 3 can be approximated using Simpson's rule with", divisions,
          "subdivisions with a value of", Simp)

    # Defines the function for part B
    fnc = lambda x: math.cos(2*x)*x**3
    # Runs the function and prints the value
    Simp, divisions = Simpson(fnc, 2, 3, 23)
    print("The integral of cos(2x)x^3 from 2 to 3 can be approximated using Simpson's rule with", divisions,
          "subdivisions with a value of", Simp)

# Calls the main() function
main()