"""
Luke Vaughan
2-3-20
MAE 3403
HW2
"""
"""
Write a function defined as: def Secant(fcn, x0, x1, maxiter=10,xtol=1e-5)

Purpose: use the secant method to find the root of fcn(x), in the neighborhood of x0, x1.

fcn: the function for which we want to find the root
x0 and x1: two values in the neighborhood of the root
xtol: exit if the |x_newest - x_previous| < xtol
maxiter: exit if the number of iterations (new x values) equals this number
return value: the final estimate of the root (most recent new x value)
"""

import math

def Secant(fcn, x0, x1, maxiter, xtol):
    iter = 0
    xn = x0
    xnminus1 = x1
    xnplus1 = 0

    while abs(xn - xnminus1) > xtol and iter <= maxiter:
        xnplus1 = xn - fcn(xn) * (xn - xnminus1) / (fcn(xn) - fcn(xnminus1))
        xnminus1 = xn
        xn = xnplus1
        iter += 1
    return xn


def main():
    fcn = lambda x: x - 3 * math.cos(x)
    zero = Secant(fcn, 1, 2, 5, 1e-4)
    print("In the neighborhood of 1 and 2, for y = x - 3cos(x), max number of iterations of 5, with 1e-4 tolerance: "
          "there exists a zero at x =", zero)

    fcn = lambda x: math.cos(2 * x) * x ** 3
    zero = Secant(fcn, 1, 2, 15, 1e-8)
    print("In the neighborhood of 1 and 2, for y = cos(2x)x^3, max number of iterations of 15, with 1e-8 tolerance: "
          "there exists a zero at x =", zero)

    zero = Secant(fcn, 1, 2, 3, 1e-8)
    print("In the neighborhood of 1 and 2, for y = cos(2x)x^3, max number of iterations of 3, with 1e-8 tolerance: "
          "there exists a zero at x =", zero)


main()
