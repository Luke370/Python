"""
Luke Vaughan
2-3-20
MAE 3403
HW2
"""
"""
Write a function defined as: def GaussElim(Aaug):

Purpose of GaussElim(Aaug): use the Gauss Elimination with Back Substitution method to solve the set of linear 
equations expressed in matrix form as A x = b. Both A and b are contained in the function argument - Aaug. 

Aaug: an augmented matrix containing [A | b] having N rows and N+1 columns, where N is the number of equations in the 
set.
return value: x - an array of size N, containing the solution to A x = b

Notes: Do NOT use partial pivoting in this program. (I want everyone to get the exact same answer, and I don't want you 
working quite that hard.)

Write and call a main() function that uses your GaussElim function to calculate and print the solution to the same set 
of linear equations given in part c).
"""
def GaussElim(Aaug):
    const = 0
    list = []
    x = [0] * len(Aaug)
    for i in range(len(Aaug)):
        const = 1 / Aaug[i][i]
        Aaug[i] = [x * const for x in Aaug[i]]
        for j in range(i+1,len(Aaug[i])-1):
            const = -1 * Aaug[j][i]
            list = [x * const for x in Aaug[i]]
            Aaug[j] = [list + Aaug[j] for list, Aaug[j] in zip(list, Aaug[j])]

    # Matrix is now in row echlon form and ready for back sub
    # i iterates through the rows and counts backwards
    x[len(Aaug)-1] = Aaug[i][len(Aaug[i]) - 1]
    for i in range(len(Aaug)-2,-1,-1):
        mysum = 0
        # j iterates through the columns and counts forwards
        for j in range(i,len(Aaug[i])-2):
            mysum += Aaug[i][j+1]*x[j+1]
        x[i] = Aaug[i][len(Aaug[i])-1] - mysum
    return x

def main():
    MyA = [[4, -1, -1, 3],[-2, -3, 1, 9],[-1, 1, 7, -6]]
    x = GaussElim(MyA)
    print("After performing Gaussian Elimination, our values are:", x)
    anotherA = [[4, 3, 1, -1, 2],[2, -5, 0, -2, -3],[-3, 3, -6, 1, 5],[0, 1, 4, 8, -2]]
    x = GaussElim(anotherA)
    print("After performing Gaussian Elimination, our values are:", x)

main()