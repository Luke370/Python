"""
Luke Vaughan
2-3-20
MAE 3403
HW2
"""
"""
Write a function defined as: def GaussSeidel(Aaug, x, Niter = 15):

Purpose: use the Gauss-Seidel method to estimate the solution to a set of N linear equations expressed in matrix form 
as A x = b. Both A and b are contained in the function argument - Aaug. 

Aaug: an augmented matrix containing [A |b] having N rows and N+1 columns, where N is the number of equations in the set.
x: a vector (array) contain the values of the initial guess
Niter: the number of iterations (new x vectors) to compute
return value: the final new x vector
"""

def GaussSeidel(Aaug, x, Niter):
    # Iterates the correct number of times
    for i in range(Niter):
        # Iterates through the Rows of the Matrix
        for j in range(len(Aaug)):
            # Resets the value of mysum for the next iteration
            mysum = 0
            # Iterates through the columns of the Matrix to add the coefficients with their x-values
            for m in range(len(Aaug[j])-1):
                # Skips the value that we are currently on
                if j == m:
                    continue
                # Adds all the values that we need
                else:
                    mysum += Aaug[j][m]*x[m]
            # Computes the next value of x and proceeds to iterate again
            x[j] = (1/Aaug[j][j]) * (Aaug[j][len(Aaug[j])-1] - mysum)
    return x

def main():
    # With an initial guess of all zeros. Perform 22 iterations.
    MyA = [[4, -1, -1, 3],[-2, -3, 1, 9],[-1, 1, 7, -6]]
    x = GaussSeidel(MyA, [0]*len(MyA),22)
    print("After the defined number of iterations the new values are:", x)
    # With an initial guess of all zeros. Perform 3 iterations.
    anotherA = [[4, 3, 1, -1, 2],[2, -5, 0, -2, -3],[-3, 3, -6, 1, 5],[0, 1, 4, 8, -2]]
    x = GaussSeidel(anotherA, [0] * len(anotherA), 3)
    print("After the defined number of iterations the new values are:", x)

main()


